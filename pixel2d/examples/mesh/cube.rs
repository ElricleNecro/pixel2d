use nalgebra as na;

use pixel_2d::render::Program;
use pixel_2d::render::buffer::{ArrayBuffer, ElementArrayBuffer, VertexArray};
use pixel_2d::resources::{Error as RError, Resources};

use super::{Renderable, Vertex};

pub struct Cube {
    program: Program,
    vbo: ArrayBuffer,
    ibo: ElementArrayBuffer,
    vao: VertexArray,

    pub model: na::Matrix4<f32>,

    vertices: Vec<Vertex>,
    indices: Vec<pixel2d_gl::types::GLuint>,

    time: f32,
}

impl Cube {
    pub fn new(res: &Resources, length: f32) -> Result<Self, RError> {
        let mut out: Cube = Default::default();

        out.vertices = vec![
            // front
            Vertex { pos: (-length, -length,  length).into(), color: (1.0, 0.0, 0.0, 0.7).into() },
            Vertex { pos: ( length, -length,  length).into(), color: (0.0, 1.0, 0.0, 0.7).into() },
            Vertex { pos: ( length,  length,  length).into(), color: (0.0, 0.0, 1.0, 0.7).into() },
            Vertex { pos: (-length,  length,  length).into(), color: (1.0, 1.0, 1.0, 0.7).into() },
            // back
            Vertex { pos: (-length, -length, -length).into(), color: (1.0, 0.0, 0.0, 0.7).into() },
            Vertex { pos: ( length, -length, -length).into(), color: (0.0, 1.0, 0.0, 0.7).into() },
            Vertex { pos: ( length,  length, -length).into(), color: (0.0, 0.0, 1.0, 0.7).into() },
            Vertex { pos: (-length,  length, -length).into(), color: (1.0, 1.0, 1.0, 0.7).into() },
        ];

        out.indices = vec![
            // front
            0, 1, 2,
            2, 3, 0,
            // right
            1, 5, 6,
            6, 2, 1,
            // back
            7, 6, 5,
            5, 4, 7,
            // left
            4, 0, 3,
            3, 7, 4,
            // bottom
            4, 5, 1,
            1, 0, 4,
            // top
            3, 2, 6,
            6, 7, 3
        ];

        out.vbo = ArrayBuffer::new();
        out.vbo.bind();
        out.vbo.static_draw(&out.vertices);
        out.vbo.unbind();

        out.ibo = ElementArrayBuffer::new();
        out.ibo.bind();
        out.ibo.static_draw(&out.indices);
        out.ibo.unbind();

        out.vao = VertexArray::new();
        out.vao.bind();
        out.vbo.bind();
        out.ibo.bind();
        Vertex::vertex_attrib_pointers();
        out.ibo.unbind();
        out.vbo.unbind();
        out.vao.unbind();

        out.program = Program::from_resources(res, "cube")?;

        Ok(out)
    }
}

impl Renderable for Cube {
    fn translate(&mut self, trans: &na::Matrix4<f32>) {
        self.model = trans * self.model;
    }

    fn render(&mut self, context: *const pixel2d_gl:: Gl, projection: &na::Matrix4<f32>, view: &na::Matrix4<f32>, elapsed: f32) -> Result<(), RError> {
        self.time += elapsed;

        unsafe {
            (*context).Enable(pixel2d_gl::DEPTH_TEST);
            (*context).Enable(pixel2d_gl::CULL_FACE);
            (*context).Enable(pixel2d_gl::BLEND);
            (*context).BlendFunc(pixel2d_gl::SRC_ALPHA, pixel2d_gl::ONE_MINUS_SRC_ALPHA);
            (*context).PolygonMode(pixel2d_gl::FRONT_AND_BACK, pixel2d_gl::FILL);
        }

        let angle = (self.time * 45.) % 360.;
        let model = self.model * na::UnitQuaternion::from_axis_angle(&na::Vector3::z_axis(), angle.to_radians()).to_homogeneous();
        // let model: na::Matrix4<f32> = na::Matrix4::identity();

        self.program.bind();
        self.vao.bind();

        let proj_loc = self.program.uniform_location("projection")?;
        let movw_loc = self.program.uniform_location("modelview")?;

        unsafe {
            (*context).UniformMatrix4fv(proj_loc, 1, pixel2d_gl::FALSE, projection.as_slice().as_ptr());
            (*context).UniformMatrix4fv(movw_loc, 1, pixel2d_gl::FALSE, (view * model).as_slice().as_ptr());
            (*context).DrawElements(
                pixel2d_gl::TRIANGLES,
                self.indices.len() as i32,
                pixel2d_gl::UNSIGNED_INT,
                self.indices.as_ptr() as *const pixel2d_gl::types::GLvoid
            );
        }

        self.vao.unbind();
        self.program.unbind();

        unsafe {
            (*context).Disable(pixel2d_gl::DEPTH_TEST);
            (*context).Disable(pixel2d_gl::CULL_FACE);
            (*context).Disable(pixel2d_gl::BLEND);
        }

        Ok(())
    }
}

impl Default for Cube {
    fn default() -> Self {
        Cube {
            program: Default::default(),

            vbo: Default::default(),
            ibo: Default::default(),
            vao: Default::default(),

            model: na::Matrix4::identity(),

            vertices: Default::default(),
            indices: Default::default(),

            time: Default::default(),
        }
    }
}
