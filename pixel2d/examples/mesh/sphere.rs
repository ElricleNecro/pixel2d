use std::collections::HashMap;

use log::debug;

use nalgebra as na;

use pixel_2d::render::Program;
use pixel_2d::render::buffer::{ArrayBuffer, ElementArrayBuffer, VertexArray};
use pixel_2d::resources::{Error as RError, Resources};

use super::{Renderable, Vertex};

const X: f32 = 0.525_731_1;
const Z: f32 = 0.850_650_8;
const N: f32 = 0.0;

type LookUp = std::collections::HashMap<(u32, u32), u32>;

pub struct IcoSphere {
    program: Program,
    vbo: ArrayBuffer,
    ibo: ElementArrayBuffer,
    vao: VertexArray,

    pub model: na::Matrix4<f32>,

    vertices: Vec<Vertex>,
    indices: Vec<u32>,

    time: f32,
}

impl IcoSphere {
    pub fn icosphere_old(res: &Resources) -> Result<Self, RError> {
        let mut out: IcoSphere = Default::default();
        let t: f32 = (1.0_f32 + 5.0_f32.sqrt()) / 2.0_f32;

        out.vertices = vec![
            Vertex { pos: (-1.,  t, 0.).into(),  color: (1.0, 0.0, 0.0, 0.7).into() },
            Vertex { pos: ( 1.,  t, 0.).into(),  color: (0.0, 1.0, 0.0, 0.7).into() },
            Vertex { pos: (-1., -t, 0.).into(),  color: (0.0, 0.0, 1.0, 0.7).into() },
            Vertex { pos: ( 1., -t, 0.).into(),  color: (1.0, 1.0, 1.0, 0.7).into() },

            Vertex { pos: ( 0., -1.,  t).into(), color: (1.0, 0.0, 0.0, 0.7).into() },
            Vertex { pos: ( 0.,  1.,  t).into(), color: (0.0, 1.0, 0.0, 0.7).into() },
            Vertex { pos: ( 0., -1., -t).into(), color: (0.0, 0.0, 1.0, 0.7).into() },
            Vertex { pos: ( 0.,  1., -t).into(), color: (1.0, 1.0, 1.0, 0.7).into() },

            Vertex { pos: ( t, 0., -1.).into(),  color: (1.0, 0.0, 0.0, 0.7).into() },
            Vertex { pos: ( t, 0.,  1.).into(),  color: (0.0, 1.0, 0.0, 0.7).into() },
            Vertex { pos: (-t, 0., -1.).into(),  color: (0.0, 0.0, 1.0, 0.7).into() },
            Vertex { pos: (-t, 0.,  1.).into(),  color: (1.0, 1.0, 1.0, 0.7).into() },
        ];

        out.indices = vec![
            0, 11, 5,
            0, 5, 1,
            0, 1, 7,
            0, 7, 10,
            0, 10, 11,

            1, 5, 9,
            5, 11, 4,
            11, 10, 2,
            10, 7, 6,
            7, 1, 8,

            3, 9, 4,
            3, 4, 2,
            3, 2, 6,
            3, 6, 8,
            3, 8, 9,

            4, 9, 5,
            2, 4, 11,
            6, 2, 10,
            8, 6, 7,
            9, 8, 1,
        ];

        out.vbo = ArrayBuffer::new();
        out.vbo.bind();
        out.vbo.static_draw(&out.vertices);
        out.vbo.unbind();

        out.ibo = ElementArrayBuffer::new();
        out.ibo.bind();
        out.ibo.static_draw(&out.indices);
        out.ibo.unbind();

        out.vao = VertexArray::new();
        out.vao.bind();
        out.vbo.bind();
        out.ibo.bind();
        Vertex::vertex_attrib_pointers();
        out.ibo.unbind();
        out.vbo.unbind();
        out.vao.unbind();

        out.program = Program::from_resources(res, "sphere")?;

        Ok(out)
    }

    pub fn icosphere(res: &Resources) -> Result<Self, RError> {
        let mut out: Self = Self {
            program: Program::from_resources(res, "sphere")?,

            vbo: ArrayBuffer::new(),
            ibo: ElementArrayBuffer::new(),
            vao: VertexArray::new(),

            model: na::Matrix4::identity(),

            vertices: Vec::new(),
            indices: vec![
                 0,  4,  1,      0,  9,  4,      9,  5,  4,      4,  5,  8,      4,  8,  1,
                 8, 10,  1,      8,  3, 10,      5,  3,  8,      5,  2,  3,      2,  7,  3,
                 7, 10,  3,      7,  6, 10,      7, 11,  6,     11,  0,  6,      0,  1,  6,
                 6,  1, 10,      9,  0, 11,      9, 11,  2,      9,  2,  5,      7,  2, 11
            ],

            time: 0.,
        };

        out.vertices.push(Vertex { pos: (-X,  N,  Z).into(), color: (1.0, 0.0, 0.0, 0.7).into() });
        out.vertices.push(Vertex { pos: ( X,  N,  Z).into(), color: (0.0, 1.0, 0.0, 0.7).into() });
        out.vertices.push(Vertex { pos: (-X,  N, -Z).into(), color: (0.0, 0.0, 1.0, 0.7).into() });
        out.vertices.push(Vertex { pos: ( X,  N, -Z).into(), color: (0.4, 0.4, 0.4, 0.7).into() });

        out.vertices.push(Vertex { pos: ( N,  Z,  X).into(), color: (1.0, 0.0, 0.0, 0.7).into() });
        out.vertices.push(Vertex { pos: ( N,  Z, -X).into(), color: (0.0, 1.0, 0.0, 0.7).into() });
        out.vertices.push(Vertex { pos: ( N, -Z,  X).into(), color: (0.0, 0.0, 1.0, 0.7).into() });
        out.vertices.push(Vertex { pos: ( N, -Z, -X).into(), color: (0.8, 0.2, 0.4, 0.7).into() });

        out.vertices.push(Vertex { pos: ( Z,  X,  N).into(), color: (1.0, 0.0, 0.0, 0.7).into() });
        out.vertices.push(Vertex { pos: (-Z,  X,  N).into(), color: (0.0, 1.0, 0.0, 0.7).into() });
        out.vertices.push(Vertex { pos: ( Z, -X,  N).into(), color: (0.0, 0.0, 1.0, 0.7).into() });
        out.vertices.push(Vertex { pos: (-Z, -X,  N).into(), color: (0.5, 0.1, 0.7, 0.7).into() });

        out.vbo.bind();
        out.vbo.static_draw(&out.vertices);
        out.vbo.unbind();

        out.ibo.bind();
        out.ibo.static_draw(&out.indices);
        out.ibo.unbind();

        out.vao.bind();
        out.vbo.bind();
        out.ibo.bind();
        Vertex::vertex_attrib_pointers();
        out.ibo.unbind();
        out.vbo.unbind();
        out.vao.unbind();

        Ok(out)
    }

    fn normalise(vertex: Vertex) -> Vertex {
        vertex
    }

    fn subdivide_edge(&mut self, lookup: &mut LookUp, first: u32, second: u32) -> u32 {
        let mut first = first;
        let mut second = second;

        if first > second {
            std::mem::swap(&mut first, &mut second);
        }

        match lookup.get(&(first, second)) {
            Some(_) => {},
            None => {
                lookup.insert((first, second), self.vertices.len() as u32);
                let edge0 = self.vertices[first as usize];
                let edge1 = self.vertices[second as usize];
                let point = Self::normalise(edge0 + edge1);

                self.vertices.push(point);
            },
        };

        *lookup.get(&(first, second)).unwrap_or(&0_u32)
    }

    pub fn subdivide(&mut self) {
        let mut lookup: LookUp = HashMap::new();
        let mut new_indices: Vec<u32> = Vec::new();
        let tmp = self.indices.clone();
        let triplet = tmp.iter()
            .step_by(3)
            .zip(
                tmp.iter()
                    .skip(1)
                    .step_by(3)
                    .zip(
                        tmp.iter()
                            .skip(2)
                            .step_by(3)
                    )
            )
            .map(|x| [x.0, (x.1).0, (x.1).1])
            ;

        for each in triplet {
            let mut mid = [0_u32; 3];
            for edge in 0..3 {
                mid[edge] = self.subdivide_edge(
                    &mut lookup,
                    *each[edge],
                    *each[(edge + 1) % 3]
                );
            }

            new_indices.push(*each[0]);
            new_indices.push(mid[0]);
            new_indices.push(mid[2]);

            new_indices.push(*each[1]);
            new_indices.push(mid[1]);
            new_indices.push(mid[0]);

            new_indices.push(*each[2]);
            new_indices.push(mid[2]);
            new_indices.push(mid[1]);

            new_indices.push(mid[0]);
            new_indices.push(mid[1]);
            new_indices.push(mid[2]);
        }

        self.indices = new_indices;
    }
}

impl Renderable for IcoSphere {
    fn translate(&mut self, trans: &na::Matrix4<f32>) {
        self.model = trans * self.model;
    }

    fn render(&mut self, context: *const pixel2d_gl:: Gl, projection: &na::Matrix4<f32>, view: &na::Matrix4<f32>, elapsed: f32) -> Result<(), RError> {
        self.time += elapsed;

        unsafe {
            (*context).PolygonMode(pixel2d_gl::FRONT_AND_BACK, pixel2d_gl::FILL);
            (*context).BlendFunc(pixel2d_gl::SRC_ALPHA, pixel2d_gl::ONE_MINUS_SRC_ALPHA);
            (*context).Enable(pixel2d_gl::DEPTH_TEST);
            (*context).Enable(pixel2d_gl::CULL_FACE);
            (*context).Enable(pixel2d_gl::BLEND);
        }

        let angle = (self.time * 30.) % 360.;
        let model = self.model * na::UnitQuaternion::from_axis_angle(&na::Vector3::z_axis(), angle.to_radians()).to_homogeneous();

        self.program.bind();
        self.vao.bind();

        debug!("{} -- Vertices: {:?}.", module_path!(), self.vertices);
        debug!("{} -- Indices : {:?}.", module_path!(), self.indices);

        let proj_loc = self.program.uniform_location("projection")?;
        let movw_loc = self.program.uniform_location("modelview")?;

        unsafe {
            (*context).UniformMatrix4fv(proj_loc, 1, pixel2d_gl::FALSE, projection.as_slice().as_ptr());
            (*context).UniformMatrix4fv(movw_loc, 1, pixel2d_gl::FALSE, (view * model).as_slice().as_ptr());
            (*context).DrawElements(
                pixel2d_gl::TRIANGLES,
                self.indices.len() as i32,
                pixel2d_gl::UNSIGNED_INT,
                // std::ptr::null_mut()
                self.indices.as_ptr() as *const pixel2d_gl::types::GLvoid
            );
        }

        self.vao.unbind();
        self.program.unbind();

        unsafe {
            (*context).Disable(pixel2d_gl::DEPTH_TEST);
            (*context).Disable(pixel2d_gl::CULL_FACE);
            (*context).Disable(pixel2d_gl::BLEND);
        }

        Ok(())
    }
}

impl Default for IcoSphere {
    fn default() -> Self {
        IcoSphere {
            program: Default::default(),

            vbo: Default::default(),
            ibo: Default::default(),
            vao: Default::default(),

            model: na::Matrix4::identity(),

            vertices: Default::default(),
            indices: Default::default(),

            time: Default::default(),
        }
    }
}
