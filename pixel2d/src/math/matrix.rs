use std::fmt::{Display, Error as FmtError, Formatter};

use std::iter::{FromIterator, IntoIterator};

use std::ops::{Add, AddAssign};
use std::ops::{Index, IndexMut};
use std::ops::{Mul, MulAssign};
use std::ops::{Sub, SubAssign};

use super::Vector;

use log::debug;

#[derive(Copy,Clone,Debug)]
pub struct Matrix([f32; 16]);

impl Matrix {
    #[inline]
    pub fn flat_index(x: usize, y:usize) -> usize {
        x * 4 + y
    }

    #[inline]
    pub fn unflattened_index(idx: usize) -> (usize, usize) {
        (idx / 4, idx % 4)
    }

    pub fn perspective(fov: f32, ratio: f32, near: f32, far: f32) -> Self {
        let mut persp = Matrix([0.; 16]);

        let f: f32 = 1. / (fov / 2.0_f32).to_radians().tan();

        // persp.0[Matrix::flat_index(0, 0)] = f / ratio;
        // persp.0[Matrix::flat_index(1, 1)] = f;
        // persp.0[Matrix::flat_index(2, 2)] = (near + far) / (near - far);
        // persp.0[Matrix::flat_index(2, 3)] = 2. * near * far / (near - far);
        // persp.0[Matrix::flat_index(3, 2)] = -1.;
        persp.0[Matrix::flat_index(0, 0)] = f / ratio;
        persp.0[Matrix::flat_index(1, 1)] = f;
        persp.0[Matrix::flat_index(2, 2)] = far / (far - near);
        persp.0[Matrix::flat_index(3, 2)] = -near * far / (far - near);
        persp.0[Matrix::flat_index(2, 3)] = 1.;
        persp.0[Matrix::flat_index(3, 3)] = 0.;

        /*
         *    float frustumDepth = farDist - nearDist;
    float oneOverDepth = 1 / frustumDepth;

    result[1][1] = 1 / tan(0.5f * fov);
    result[0][0] = (leftHanded ? 1 : -1 ) * result[1][1] / aspect;
    result[2][2] = farDist * oneOverDepth;
    result[3][2] = (-farDist * nearDist) * oneOverDepth;
    result[2][3] = 1;
    result[3][3] = 0;*/

        persp
    }

    pub fn look_at(position: Vector, target: Vector, up: Vector) -> Self {
        let mut lt = Matrix([0.; 16]);

        let regard = target - position;
        let normal = regard * up;
        let n_axe  = normal * regard;

        lt.0[Matrix::flat_index(0, 0)] = normal[0] / normal.norm();
        lt.0[Matrix::flat_index(0, 1)] = normal[1] / normal.norm();
        lt.0[Matrix::flat_index(0, 2)] = normal[2] / normal.norm();

        lt.0[Matrix::flat_index(1, 0)] = n_axe[0] / n_axe.norm();
        lt.0[Matrix::flat_index(1, 1)] = n_axe[1] / n_axe.norm();
        lt.0[Matrix::flat_index(1, 2)] = n_axe[2] / n_axe.norm();

        lt.0[Matrix::flat_index(2, 0)] = -regard[0] / regard.norm();
        lt.0[Matrix::flat_index(2, 1)] = -regard[1] / regard.norm();
        lt.0[Matrix::flat_index(2, 2)] = -regard[2] / regard.norm();

        lt.0[Matrix::flat_index(3, 3)] = 1.;

        debug!("{} - normal : {}.", module_path!(), normal);
        debug!("{} - n_axe : {}.", module_path!(), n_axe);
        debug!("{} - regard : {}.", module_path!(), regard);
        debug!("{} - look-at before translation: {}.", module_path!(), lt);

        lt * Matrix::translation(-position)
    }

    pub fn translation(trans: Vector) -> Self {
        let mut res = Matrix::identity();

        for i in 0..3 {
            res[Matrix::flat_index(i, 3)] = trans[i];
        }

        res
    }

    pub fn quaternion(angle: f32, axis: Vector) -> Self {
        let mut mat = Matrix::zero();

        let axe = axis / axis.norm();
        // angle *= D2R
        let cos = angle.to_radians().cos();
        let sin = angle.to_radians().sin();
        let onecos = 1. - cos;
        let a01 = axe[0] * axe[1] * onecos;
        let a02 = axe[0] * axe[2] * onecos;
        let a12 = axe[1] * axe[2] * onecos;
        let a0s = axe[0] * sin;
        let a1s = axe[1] * sin;
        let a2s = axe[2] * sin;

        mat[Matrix::flat_index(0, 0)] = axe[0] * axe[0] * onecos + cos;
        mat[Matrix::flat_index(0, 1)] = a01 - a2s;
        mat[Matrix::flat_index(0, 2)] = a02 + a1s;

        mat[Matrix::flat_index(1, 0)] = a01 + a2s;
        mat[Matrix::flat_index(1, 1)] = axe[1] * axe[1] * onecos + cos;
        mat[Matrix::flat_index(1, 2)] = a12 - a0s;

        mat[Matrix::flat_index(2, 0)] = a02 - a1s;
        mat[Matrix::flat_index(2, 1)] = a12 + a0s;
        mat[Matrix::flat_index(2, 2)] = axe[2] * axe[2] * onecos + cos;

        mat[Matrix::flat_index(3, 3)] = 1.0;

        mat
    }

    pub fn zero() -> Self {
        Matrix([0.; 16])
    }

    pub fn identity() -> Self {
        Default::default()
    }

    pub fn as_ptr(&self) -> *const f32 {
        self.0.as_ptr()
    }
}

impl Default for Matrix {
    fn default() -> Self {
        let mut def = Matrix([0.; 16]);

        def.0[Matrix::flat_index(0, 0)] = 1.;
        def.0[Matrix::flat_index(1, 1)] = 1.;
        def.0[Matrix::flat_index(2, 2)] = 1.;
        def.0[Matrix::flat_index(3, 3)] = 1.;

        def
    }
}

impl FromIterator<f32> for Matrix {
    fn from_iter<T>(iter: T) -> Self where T: IntoIterator<Item = f32> {
        let mut m = Matrix([0.; 16]);

        for (idx, value) in iter.into_iter().enumerate() {
            m.0[idx] = value;
        }

        m
    }
}

impl Add<Matrix> for Matrix {
    type Output = Matrix;

    fn add(self, right: Matrix) -> Self::Output {
        self.0.iter()
            .zip(right.0.iter())
            .map(|(a, b)| a + b)
            .collect()
    }
}

impl AddAssign<Matrix> for Matrix {
    fn add_assign(&mut self, right: Matrix) {
        for (idx, value) in right.0.iter().enumerate() {
            self.0[ idx ] += value;
        }
    }
}

impl Add<&Matrix> for Matrix {
    type Output = Matrix;

    fn add(self, right: &Matrix) -> Self::Output {
        self.0.iter()
            .zip(right.0.iter())
            .map(|(a, b)| a + b)
            .collect()
    }
}

impl AddAssign<&Matrix> for Matrix {
    fn add_assign(&mut self, right: &Matrix) {
        for (idx, value) in right.0.iter().enumerate() {
            self.0[ idx ] += value;
        }
    }
}

impl Sub<Matrix> for Matrix {
    type Output = Matrix;

    fn sub(self, right: Matrix) -> Self::Output {
        self.0.iter()
            .zip(right.0.iter())
            .map(|(a, b)| a - b)
            .collect()
    }
}

impl SubAssign<Matrix> for Matrix {
    fn sub_assign(&mut self, right: Matrix) {
        for (idx, value) in right.0.iter().enumerate() {
            self.0[ idx ] -= value;
        }
    }
}

impl Sub<&Matrix> for Matrix {
    type Output = Matrix;

    fn sub(self, right: &Matrix) -> Self::Output {
        self.0.iter()
            .zip(right.0.iter())
            .map(|(a, b)| a - b)
            .collect()
    }
}

impl SubAssign<&Matrix> for Matrix {
    fn sub_assign(&mut self, right: &Matrix) {
        for (idx, value) in right.0.iter().enumerate() {
            self.0[ idx ] -= value;
        }
    }
}

impl Mul<Matrix> for Matrix {
    type Output = Matrix;

    fn mul(self, right: Matrix) -> Self::Output {
        let mut res = Matrix([0.; 16]);

        for i in 0..4 {
            for j in 0..4 {
                res.0[ Matrix::flat_index(i, j) ] = self.0.iter()
                    .enumerate()
                    .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                    .filter(|((x, _), _)| *x == i)
                    .map(|(_, v)| v)
                    .zip(
                        right.0.iter()
                        .enumerate()
                        .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                        .filter(|((_, y), _)| *y == j)
                        .map(|(_, v)| v)
                    )
                    .map(|(a, b)| a * b)
                    .sum();
            }
        }

        res
    }
}

impl MulAssign<Matrix> for Matrix {
    fn mul_assign(&mut self, right: Matrix) {
        for i in 0..4 {
            for j in 0..4 {
                self.0[ Matrix::flat_index(i, j) ] = self.0.iter()
                    .enumerate()
                    .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                    .filter(|((x, _), _)| *x == i)
                    .map(|(_, v)| v)
                    .zip(
                        right.0.iter()
                        .enumerate()
                        .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                        .filter(|((_, y), _)| *y == j)
                        .map(|(_, v)| v)
                    )
                    .map(|(a, b)| a * b)
                    .sum();
            }
        }
    }
}

impl Mul<&Matrix> for Matrix {
    type Output = Matrix;

    fn mul(self, right: &Matrix) -> Self::Output {
        let mut res = Matrix([0.; 16]);

        for i in 0..4 {
            for j in 0..4 {
                res.0[ Matrix::flat_index(i, j) ] = self.0.iter()
                    .enumerate()
                    .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                    .filter(|((x, _), _)| *x == i)
                    .map(|(_, v)| v)
                    .zip(
                        right.0.iter()
                        .enumerate()
                        .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                        .filter(|((_, y), _)| *y == j)
                        .map(|(_, v)| v)
                    )
                    .map(|(a, b)| a * b)
                    .sum();
            }
        }

        res
    }
}

impl MulAssign<&Matrix> for Matrix {
    fn mul_assign(&mut self, right: &Matrix) {
        for i in 0..4 {
            for j in 0..4 {
                self.0[ Matrix::flat_index(i, j) ] = self.0.iter()
                    .enumerate()
                    .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                    .filter(|((x, _), _)| *x == i)
                    .map(|(_, v)| v)
                    .zip(
                        right.0.iter()
                        .enumerate()
                        .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                        .filter(|((_, y), _)| *y == j)
                        .map(|(_, v)| v)
                    )
                    .map(|(a, b)| a * b)
                    .sum();
            }
        }
    }
}

impl Mul<Matrix> for &Matrix {
    type Output = Matrix;

    fn mul(self, right: Matrix) -> Self::Output {
        let mut res = Matrix([0.; 16]);

        for i in 0..4 {
            for j in 0..4 {
                res.0[ Matrix::flat_index(i, j) ] = self.0.iter()
                    .enumerate()
                    .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                    .filter(|((x, _), _)| *x == i)
                    .map(|(_, v)| v)
                    .zip(
                        right.0.iter()
                        .enumerate()
                        .map(|(idx, v)| (Matrix::unflattened_index(idx), v))
                        .filter(|((_, y), _)| *y == j)
                        .map(|(_, v)| v)
                    )
                    .map(|(a, b)| a * b)
                    .sum();
            }
        }

        res
    }
}

impl Mul<Vector> for Matrix {
    type Output = Vector;

    fn mul(self, right: Vector) -> Self::Output {
        let mut res = Vector::zero();

        for i in 0..3 {
            res[i] = right[0] * self.0[ Matrix::flat_index(i, 0) ] +
                     right[0] * self.0[ Matrix::flat_index(i, 1) ] +
                     right[0] * self.0[ Matrix::flat_index(i, 2) ] +
                     self.0[ Matrix::flat_index(i, 3) ];
        }

        res
    }
}

impl Mul<f32> for Matrix {
    type Output = Matrix;

    fn mul(self, right: f32) -> Self::Output {
        let mut res = Matrix([0.; 16]);

        for i in 0..16 {
            res.0[i] = self.0[i] * right;
        }

        res
    }
}

impl MulAssign<f32> for Matrix {
    fn mul_assign(&mut self, right: f32) {
        for i in 0..16 {
            self.0[i] *= right;
        }
    }
}

impl Mul<Matrix> for f32 {
    type Output = Matrix;

    fn mul(self, right: Matrix) -> Self::Output {
        let mut res = Matrix([0.; 16]);

        for i in 0..16 {
            res.0[i] = right.0[i] * self;
        }

        res
    }
}

impl Index<usize> for Matrix {
    type Output = f32;

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[ index ]
    }
}

impl IndexMut<usize> for Matrix {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[ index ]
    }
}

impl Display for Matrix {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        write!(f, "[[")?;
        write!(f, "{}", self.0[0])?;
        for i in 1..16 {
            write!(f, ", {}", self.0[i])?;
        }
        write!(f, "]]")
    }
}

#[cfg(test)]
mod tests {
    use super::Matrix;

    #[test]
    fn mat_mul() {
        let id = Matrix::identity();
        let mut m  = Matrix::zero();

        m[Matrix::flat_index(0, 0)] = 2.;
        m[Matrix::flat_index(0, 1)] = 3.;
        m[Matrix::flat_index(0, 2)] = 0.;
        m[Matrix::flat_index(0, 3)] = 4.;

        m[Matrix::flat_index(1, 0)] = 1.;
        m[Matrix::flat_index(1, 1)] = 2.;
        m[Matrix::flat_index(1, 2)] = 3.;
        m[Matrix::flat_index(1, 3)] = 4.;

        m[Matrix::flat_index(2, 0)] = 5.;
        m[Matrix::flat_index(2, 1)] = 6.;
        m[Matrix::flat_index(2, 2)] = 7.;
        m[Matrix::flat_index(2, 3)] = 8.;

        m[Matrix::flat_index(3, 0)] = 9.;
        m[Matrix::flat_index(3, 1)] = 8.;
        m[Matrix::flat_index(3, 2)] = 7.;
        m[Matrix::flat_index(3, 3)] = 6.;

        let res = id * m;

        for i in 0..16 {
            assert_eq!(res[i], m[i]);
        }
    }

    #[test]
    fn mat_mulassign() {
        let id = Matrix::identity();
        let mut m  = Matrix::zero();
        let mut res = Matrix::zero();

        m[Matrix::flat_index(0, 0)] = 2.;
        m[Matrix::flat_index(0, 1)] = 3.;
        m[Matrix::flat_index(0, 2)] = 0.;
        m[Matrix::flat_index(0, 3)] = 4.;

        m[Matrix::flat_index(1, 0)] = 1.;
        m[Matrix::flat_index(1, 1)] = 2.;
        m[Matrix::flat_index(1, 2)] = 3.;
        m[Matrix::flat_index(1, 3)] = 4.;

        m[Matrix::flat_index(2, 0)] = 5.;
        m[Matrix::flat_index(2, 1)] = 6.;
        m[Matrix::flat_index(2, 2)] = 7.;
        m[Matrix::flat_index(2, 3)] = 8.;

        m[Matrix::flat_index(3, 0)] = 9.;
        m[Matrix::flat_index(3, 1)] = 8.;
        m[Matrix::flat_index(3, 2)] = 7.;
        m[Matrix::flat_index(3, 3)] = 6.;

        res[Matrix::flat_index(0, 0)] = 2.;
        res[Matrix::flat_index(0, 1)] = 3.;
        res[Matrix::flat_index(0, 2)] = 0.;
        res[Matrix::flat_index(0, 3)] = 4.;

        res[Matrix::flat_index(1, 0)] = 1.;
        res[Matrix::flat_index(1, 1)] = 2.;
        res[Matrix::flat_index(1, 2)] = 3.;
        res[Matrix::flat_index(1, 3)] = 4.;

        res[Matrix::flat_index(2, 0)] = 5.;
        res[Matrix::flat_index(2, 1)] = 6.;
        res[Matrix::flat_index(2, 2)] = 7.;
        res[Matrix::flat_index(2, 3)] = 8.;

        res[Matrix::flat_index(3, 0)] = 9.;
        res[Matrix::flat_index(3, 1)] = 8.;
        res[Matrix::flat_index(3, 2)] = 7.;
        res[Matrix::flat_index(3, 3)] = 6.;

        m *= id;

        for i in 0..16 {
            assert_eq!(res[i], m[i]);
        }
    }
}
