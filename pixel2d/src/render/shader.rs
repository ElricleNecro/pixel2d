use std::ffi::CString;
use std::fmt::{Display, Formatter, self};
use std::path::Path;

use pixel2d_gl::types::{GLint, GLuint};

use pixel2d_gl::{VERTEX_SHADER, FRAGMENT_SHADER};

use log::{debug, error};

use crate::GL;
use crate::resources::{Resources, Error as ResourcesError, name_to_path};

#[derive(Debug)]
pub enum ShaderError {
    CString { message: String },
    Source  { message: String },
    Compile { kind: ShaderType, message: String },
    Link    { message: String },
}

impl Display for ShaderError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            ShaderError::CString {message: msg}          => write!(f, "Error while converting to CString: '{}'.", msg),
            ShaderError::Source  {message: msg}          => write!(f, "Error while processing the source: '{}'.", msg),
            ShaderError::Link    {message: msg}          => write!(f, "Error while linking the shaders: '{}'.",   msg),
            ShaderError::Compile {kind: k, message: msg} => write!(f, "Error while compiling the {} shader: '{}'.", k, msg),
        }
    }
}

macro_rules! gl_str {
    ($s:expr) => { match CString::new($s) {
        Ok(s) => s,
        Err(e) => { return Err(ShaderError::CString { message: e.to_string() }); }
    }.as_ptr() }
}

/// Kind of supported OpenGL shader type.
#[repr(u32)]
#[derive(Copy,Clone,Debug)]
pub enum ShaderType {
    /// For the Vertex shader.
    VERTEX   = VERTEX_SHADER,
    /// For the fragment shader.
    FRAGMENT = FRAGMENT_SHADER,
}

impl Into<GLuint> for ShaderType {
    fn into(self) -> GLuint {
        match self {
            ShaderType::VERTEX   => VERTEX_SHADER,
            ShaderType::FRAGMENT => FRAGMENT_SHADER,
        }
    }
}

impl Display for ShaderType {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ShaderType::FRAGMENT => "FRAGMENT_SHADER",
                ShaderType::VERTEX   => "VERTEX_SHADER",
            }
        )
    }
}

/// Create an empty CString used to extract errors from OpenGL logging system.
fn empty_cstring(len: usize) -> CString {
    // Allocating the vector:
    let mut buffer: Vec<u8> = Vec::with_capacity(len + 1);
    // Filling it with whitespace characters:
    buffer.extend([b' '].iter().cycle().take(len));
    // Returning a new CString:
    unsafe {
        CString::from_vec_unchecked(buffer)
    }
}

/// OpenGL shader object.
#[derive(Clone,Default)]
pub struct Shader(GLuint);

impl Shader {
    /// Create a shader object of type `kind` using the source code `source`.
    pub fn from_source(source: &str, kind: ShaderType) -> Result<Self, ShaderError> {
        // Creating the Shader on OpenGL side:
        let shader_id = unsafe {
            (*GL).CreateShader(kind.into())
        };
        // We will need CString to communicate with C:
        let c_source = match CString::new(source.as_bytes()) {
            Ok(s)  => s,
            Err(e) => { return Err(ShaderError::Source { message: e.to_string() }) },
        };
        // Will be used for error handling:
        let mut success: GLint = 1;

        // All GL function are unsafe because automatically generated from C:
        unsafe {
            // Attach the source to the newly created shader:
            (*GL).ShaderSource(shader_id, 1, &c_source.as_ptr(), std::ptr::null());
            // Compiling it:
            (*GL).CompileShader(shader_id);
            // Checking for errors:
            (*GL).GetShaderiv(shader_id, pixel2d_gl::COMPILE_STATUS, &mut success);
        }

        // Has there been any kind of error?
        if success == 0 {
            // There was an error, extracting it.
            let mut len: GLint = 0;
            unsafe {
                // Getting the length of the info log:
                (*GL).GetShaderiv(shader_id, pixel2d_gl::INFO_LOG_LENGTH, &mut len);
            }
            // Allocating an empty buffer with this size:
            let error = empty_cstring(len as usize);
            unsafe {
                // Using this buffer to extract the error message:
                (*GL).GetShaderInfoLog(
                    shader_id,
                    len,
                    std::ptr::null_mut(),
                    error.as_ptr() as *mut pixel2d_gl::types::GLchar
                );
            }

            // Returning an error:
            return Err(ShaderError::Compile {
                kind,
                message: error.to_string_lossy().into_owned()
            });
        }

        // No error, returning the shader:
        Ok(Self(shader_id))
    }

    pub fn new_vertex(source: &str) -> Result<Self, ShaderError> {
        Self::from_source(source, ShaderType::VERTEX)
    }

    pub fn new_fragment(source: &str) -> Result<Self, ShaderError> {
        Self::from_source(source, ShaderType::FRAGMENT)
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        if self.0 == 0 {
            return ;
        }

        unsafe {
            (*GL).DeleteShader(self.0);
        }
    }
}

#[derive(Default)]
pub struct Program {
    id: GLuint,
    shaders: Vec<Shader>,
}

impl Program {
    pub fn new_from_shaders(shaders: &[Shader]) -> Result<Self, ShaderError> {
        let id = unsafe {
            (*GL).CreateProgram()
        };

        for shader in shaders {
            unsafe {
                (*GL).AttachShader(id, shader.0);
            }
        }

        unsafe {
            (*GL).LinkProgram(id);
        }

        // Will be used for error handling:
        let mut success: GLint = 1;

        unsafe {
            // Checking for errors:
            (*GL).GetProgramiv(id, pixel2d_gl::LINK_STATUS, &mut success);
        }

        // Has there been any kind of error?
        if success == 0 {
            // There was an error, extracting it.
            let mut len: GLint = 0;
            unsafe {
                // Getting the length of the info log:
                (*GL).GetProgramiv(id, pixel2d_gl::INFO_LOG_LENGTH, &mut len);
            }
            // Allocating an empty buffer with this size:
            let error = empty_cstring(len as usize);
            unsafe {
                // Using this buffer to extract the error message:
                (*GL).GetProgramInfoLog(
                    id,
                    len,
                    std::ptr::null_mut(),
                    error.as_ptr() as *mut pixel2d_gl::types::GLchar
                );
            }

            // Returning an error:
            return Err(ShaderError::Link { message: error.to_string_lossy().into_owned() });
        }

        for shader in shaders {
            unsafe {
                (*GL).DetachShader(id, shader.0);
            }
        }

        Ok(Program {
            id,
            shaders: shaders.to_owned(),
        })
    }

    pub fn from_resources(res: &Resources, name: &'static str) -> Result<Self, ResourcesError> {
        let path = name_to_path(Path::new("shaders"), name);
        let mut program = Program {
            id: 0,
            shaders: Vec::new(),
        };

        program.shaders.push(
            Shader::new_vertex(&res.get(path.join("vertex.glsl"))?)?
        );

        program.shaders.push(
            Shader::new_fragment(&res.get(path.join("fragment.glsl"))?)?
        );

        program.id = unsafe {
            (*GL).CreateProgram()
        };

        for shader in &program.shaders {
            unsafe {
                (*GL).AttachShader(program.id, shader.0);
            }
        }

        unsafe {
            (*GL).LinkProgram(program.id);
        }

        // Will be used for error handling:
        let mut success: GLint = 1;

        unsafe {
            // Checking for errors:
            (*GL).GetProgramiv(program.id, pixel2d_gl::LINK_STATUS, &mut success);
        }

        // Has there been any kind of error?
        if success == 0 {
            // There was an error, extracting it.
            let mut len: GLint = 0;
            unsafe {
                // Getting the length of the info log:
                (*GL).GetProgramiv(program.id, pixel2d_gl::INFO_LOG_LENGTH, &mut len);
            }
            // Allocating an empty buffer with this size:
            let error = empty_cstring(len as usize);
            unsafe {
                // Using this buffer to extract the error message:
                (*GL).GetProgramInfoLog(
                    program.id,
                    len,
                    std::ptr::null_mut(),
                    error.as_ptr() as *mut pixel2d_gl::types::GLchar
                );
            }

            // Returning an error:
            return Err(
                ResourcesError::Shader(
                    ShaderError::Link {
                        message: error.to_string_lossy().into_owned()
                    }
                )
            );
        }

        for shader in &program.shaders {
            unsafe {
                (*GL).DetachShader(program.id, shader.0);
            }
        }

        Ok(program)
    }

    pub fn bind(&self) {
        unsafe {
            (*GL).UseProgram(self.id);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            (*GL).UseProgram(0);
        }
    }

    pub fn uniform_location(&self, name: &'static str) -> Result<GLint, ShaderError> {
        debug!("{} - Loading {} for shader {}.", module_path!(), name, self.id);
        let loc = unsafe {
            (*GL).GetUniformLocation(self.id, gl_str!(name))
        };
        debug!("{} - Uniform location of {} is {} for Shader {}.", module_path!(), name, loc, self.id);

        if loc < 0 {
            // Returning an error:
            error!("{} - Uniform location was not found for {}.", module_path!(), name);
            return Err(ShaderError::Source {
                message: format!("Uniform location was not found for {}.", name)
            });
        }

        Ok(loc)
    }
}

impl Drop for Program {
    fn drop(&mut self) {
        if self.id == 0 {
            return ;
        }

        unsafe {
            (*GL).DeleteProgram(self.id);
        }
    }
}
