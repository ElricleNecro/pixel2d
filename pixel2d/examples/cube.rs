use std::path::Path;

use pixel_2d::{PixelDrawer, Pixel2D, Camera, Events};
use pixel_2d::resources::Resources;
use pixel_2d::render::ColorBuffer;

use nalgebra as na;
use obj::ObjError;

use pixel2d_gl;

use log::error;

mod mesh;
use mesh::{Cube, sphere::IcoSphere, Renderable};

enum ExampleError {
    Io(::std::io::Error),
    Obj(ObjError),
}

impl From<::std::io::Error> for ExampleError {
    fn from(other: ::std::io::Error) -> Self {
        ExampleError::Io(other)
    }
}

impl From<ObjError> for ExampleError {
    fn from(other: ObjError) -> Self {
        ExampleError::Obj(other)
    }
}

// #[derive(Copy,Clone,Debug)]
// #[derive(VertexAttribPointers)]
// #[repr(C, packed)]
// struct MeshVertex {
    // #[location = 0]
    // pos: Vec3f32,
    // #[location = 1]
    // normal: Vec3f32,
    // #[location = 2]
    // texcoord: Vec3f32,
// }

// struct Mesh {
    // program: Program,
    // vbo: ArrayBuffer,
    // ibo: ElementArrayBuffer,
    // vao: VertexArray,

    // pub model: na::Matrix4<f32>,

    // data: Vec<MeshVertex>,
    // indices: Vec<u16>,

    // time: f32,
// }

// impl Mesh {
    // pub fn load(file: &str) -> Result<Self, ExampleError> {
        // let infile = BufReader::new(File::open(file)?);
        // let obj: Obj<TexturedVertex> = load_obj(infile)?;
        // let mut out = Self {
            // program: ,

            // vbo: ArrayBuffer::new(),
            // ibo: ElementArrayBuffer::new(),
            // vao: VertexArray::new(),

            // data: ,
            // indices: obj.indices,

            // time: 0.,
        // }

    // }
// }

struct Window {
    width: i32,
    height: i32,

    time: f32,

    camera: Camera,

    tri: Vec<Box<dyn Renderable>>,
    bg: ColorBuffer,
}

impl Window {
    pub fn new(w: i32, h: i32) -> Self {
        let mut win: Window = Default::default();

        win.width  = w;
        win.height = h;

        win.camera.set_position(na::Point3::new(2.0, 1.2, -7.0));
        win.camera.update_aspect(w as f32 / h as f32);

        win
    }
}

impl Default for Window {
    fn default() -> Self {
        Window {
            width: 640,
            height: 480,
            time: 0.,
            camera: Camera::new(16./9., ::std::f32::consts::FRAC_PI_2, 0.01, 1000., 2.0),
            tri: Vec::new(),
            bg: ColorBuffer::default(),
        }
    }
}

impl PixelDrawer for Window {
    fn on_user_create(&mut self, _context: *const pixel2d_gl::Gl) -> bool {
        let res = match Resources::from_relative_exe_path(Path::new("assets")) {
            Ok(x) => x,
            Err(e) => {
                error!("Error while loading the assets: {}.", e);
                return false;
            },
        };

        let mut tmp = match IcoSphere::icosphere(&res) {
                Ok(o) => Box::new(o),
                Err(e) => {
                    error!("Error while loading the icosphere: {}.", e);
                    return false;
                },
            };
        tmp.subdivide();
        // tmp.subdivide();
        // tmp.subdivide();
        self.tri.push(
            tmp
        );

        self.tri.push(
            match Cube::new(&res, 1.) {
                Ok(o) => Box::new(o),
                Err(e) => {
                    error!("Error while loading the cube: {}.", e);
                    return false;
                },
            }
        );

        self.tri[0].translate(&na::Matrix4::new_translation(&na::Vector3::new(-2.,  1., 0.)));
        self.tri[1].translate(&na::Matrix4::new_translation(&na::Vector3::new(2., -1., 0.)));

        self.bg = ColorBuffer::from_color(na::Vector3::new(0.3, 0.7, 1.0));
        self.bg.apply();

        true
    }

    fn on_user_update(&mut self, context: *const pixel2d_gl::Gl, _evt: &Events, elapsed: f32) -> bool {
        self.time += elapsed;

        let distance = 1. * ((self.time * 45.) % 360.).to_radians().sin();

        for model in &mut self.tri {
            model.translate(&na::Matrix4::new_translation(&na::Vector3::new(0., 0., distance)));
            match model.render(context, &self.camera.projection(), &self.camera.view2(), elapsed) {
                Ok(_) => {},
                Err(e) => {
                    error!("Render failed: '{}'.", e);
                    return false;
                }
            }
        }

        true
    }
}

pub fn main() -> Result<(), String> {
    let width = 640;
    let height = 480;

    if let Err(e) = stderrlog::new()
        .quiet(false)
        .verbosity(6)
        .timestamp(stderrlog::Timestamp::Millisecond)
        .modules(vec!["cube", "t3d"])
        .init() {
        return Err(format!("Error while creating the logger: {:?}.", e));
    }

    let mut app = Pixel2D::create()
        .title("Yipikay")
        .width(width)
        .height(height)
        // .resizable()
        .build(
            Box::new(
                Window::new(width, height)
            )
        )?;

    app.run();

    Ok(())
}
