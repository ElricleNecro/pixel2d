#version 330 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec4 Color;

uniform mat4 modelview;
uniform mat4 projection;

out vec4 outcolor;

void main()
{
	gl_Position = projection * modelview * vec4(Position, 1.0);
	outcolor = Color;
}
