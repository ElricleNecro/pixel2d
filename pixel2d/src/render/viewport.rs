use crate::GL;

use log::debug;

pub struct Viewport {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
}

impl Viewport {
    pub fn for_window(w: i32, h: i32) -> Self {
        Viewport {
            x: 0,
            y: 0,
            w,
            h,
        }
    }

    pub fn update(&mut self, w: i32, h: i32) {
        self.w = w;
        self.h = h;

        self.apply();
    }

    pub fn apply(&self) {
        debug!("{} - W: {}, H: {}.", module_path!(), self.w, self.h);
        unsafe {
            (*GL).Viewport(self.x, self.y, self.w, self.h);
        }
    }
}
