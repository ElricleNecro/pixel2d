use pixel2d_gl;
use crate::GL;

pub trait BufferType {
    const BUFFER_TYPE: pixel2d_gl::types::GLuint;
}

#[derive(Default)]
pub struct BufferArray;
impl BufferType for BufferArray {
    const BUFFER_TYPE: pixel2d_gl::types::GLuint = pixel2d_gl::ARRAY_BUFFER;
}

#[derive(Default)]
pub struct ElementArray;
impl BufferType for ElementArray {
    const BUFFER_TYPE: pixel2d_gl::types::GLuint = pixel2d_gl::ELEMENT_ARRAY_BUFFER;
}

/// Represent array buffer (VBO: virtual buffer object).
#[derive(Default)]
pub struct Buffer<B> where B: BufferType {
    vbo: pixel2d_gl::types::GLuint,
    _marker: ::std::marker::PhantomData<B>,
}

impl<B> Buffer<B> where B: BufferType {
    /// Create a new virtual buffer object (array buffer).
    pub fn new() -> Self {
        let mut vbo: pixel2d_gl::types::GLuint = 0;

        unsafe {
            (*GL).GenBuffers(1, &mut vbo);
        }

        Buffer {
            vbo,
            _marker: ::std::marker::PhantomData,
        }
    }

    /// Bind the array buffer.
    pub fn bind(&self) {
        unsafe {
            (*GL).BindBuffer(B::BUFFER_TYPE, self.vbo);
        }
    }

    /// Unbind the array buffer.
    pub fn unbind(&self) {
        unsafe {
            (*GL).BindBuffer(B::BUFFER_TYPE, 0);
        }
    }

    /// Attach the given array to the VBO using the STATIC_DRAW method.
    pub fn static_draw<T>(&self, data: &[T]) {
        unsafe {
            (*GL).BufferData(
                B::BUFFER_TYPE,                                                     // Target
                (data.len() * ::std::mem::size_of::<T>()) as pixel2d_gl::types::GLsizeiptr, // Size of data in bytes
                data.as_ptr() as *const pixel2d_gl::types::GLvoid,                          // Pointer to data
                pixel2d_gl::STATIC_DRAW,                                                    // Usage
            );
        }
    }

    pub fn len(&self) -> i32 {
        let mut size = 0;

        unsafe {
            (*GL).GetBufferParameteriv(B::BUFFER_TYPE, pixel2d_gl::BUFFER_SIZE, &mut size);
        }

        size
    }

    pub fn is_empty(&self) -> bool {
        self.len() <= 0
    }
}

impl<B> Drop for Buffer<B> where B: BufferType {
    fn drop(&mut self) {
        if self.vbo == 0 {
            return ;
        }

        unsafe {
            (*GL).DeleteBuffers(1, &self.vbo);
        }
    }
}

/// Shortcut to create an array buffer.
pub type ArrayBuffer = Buffer<BufferArray>;
/// Shortcut to create an element array buffer.
pub type ElementArrayBuffer = Buffer<ElementArray>;

#[derive(Default)]
pub struct VertexArray(pixel2d_gl::types::GLuint);

impl VertexArray {
    pub fn new() -> Self {
        let mut vao: pixel2d_gl::types::GLuint = 0;

        unsafe {
            (*GL).GenVertexArrays(1, &mut vao);
        }

        VertexArray(vao)
    }

    pub fn bind(&self) {
        unsafe {
            (*GL).BindVertexArray(self.0);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            (*GL).BindVertexArray(0);
        }
    }
}

impl Drop for VertexArray {
    fn drop(&mut self) {
        if self.0 == 0 {
            return ;
        }

        unsafe {
            (*GL).DeleteVertexArrays(1, &self.0);
        }
    }
}
