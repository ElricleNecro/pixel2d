use std::path::Path;

use pixel_2d::{PixelDrawer, Pixel2D, Camera, Events};
// use pixel_2d::math::{Matrix, Vector};
use pixel_2d::resources::{Error as RError, Resources};
use pixel_2d::render::{Program, Vec3f32, u2_u10_u10_u10_rev_float, ColorBuffer};
use pixel_2d::render::buffer::{ArrayBuffer, VertexArray};

use pixel2d_derive::VertexAttribPointers;

use nalgebra as na;

use pixel2d_gl;

use log::{debug, error};

#[derive(Copy,Clone,Debug)]
#[derive(VertexAttribPointers)]
#[repr(C, packed)]
struct Vertex {
    #[location = 0]
    pos: Vec3f32,
    #[location = 1]
    clr: u2_u10_u10_u10_rev_float,
}

#[derive(Default)]
struct Triangle {
    program: Program,
    vbo: ArrayBuffer,
    vao: VertexArray,
    time: f32,
}

impl Triangle {
    pub fn load(&mut self, res: &Resources) -> Result<(), RError> {
        self.program = Program::from_resources(res, "t3d")?;

        let vertices = vec![
            Vertex { pos: (-0.8, -0.8, 0.0).into(), clr: (1.0, 0.0, 0.0, 1.0).into() },
            Vertex { pos: ( 0.8, -0.8, 0.0).into(), clr: (0.0, 1.0, 0.0, 1.0).into() },
            Vertex { pos: ( 0.0,  0.8, 0.0).into(), clr: (0.0, 0.0, 1.0, 1.0).into() },
        ];

        self.vbo = ArrayBuffer::new();
        self.vbo.bind();
        self.vbo.static_draw(&vertices);
        self.vbo.unbind();

        self.vao = VertexArray::new();
        self.vao.bind();
        self.vbo.bind();
        Vertex::vertex_attrib_pointers();
        self.vbo.unbind();
        self.vao.unbind();

        Ok(())
    }

    pub fn render(&mut self, context: *const pixel2d_gl:: Gl, projection: &na::Matrix4<f32>, view: &na::Matrix4<f32>, elapsed: f32) -> Result<(), RError> {
        self.time += elapsed;

        unsafe {
            (*context).Enable(pixel2d_gl::DEPTH_TEST);
            (*context).Enable(pixel2d_gl::CULL_FACE);
            (*context).Enable(pixel2d_gl::BLEND);
            (*context).BlendFunc(pixel2d_gl::SRC_ALPHA, pixel2d_gl::ONE_MINUS_SRC_ALPHA);
            (*context).PolygonMode(pixel2d_gl::FRONT_AND_BACK, pixel2d_gl::FILL);
        }

        self.program.bind();
        self.vao.bind();

        let fade  = (self.time * 2. * std::f32::consts::PI / 5.).sin() / 2. + 0.7;
        let angle = (self.time * 45.) % 360.;
        let trans = (self.time * 2. * std::f32::consts::PI / 5.).sin();

        let model = (na::UnitQuaternion::from_axis_angle(&na::Vector3::z_axis(), angle.to_radians()) * na::Translation3::new(0., 0., trans)).to_homogeneous();

        let fade_loc = self.program.uniform_location("fade")?;
        let proj_loc = self.program.uniform_location("projection")?;
        let movw_loc = self.program.uniform_location("modelview")?;


        unsafe {
            // (*context).UniformMatrix4fv(proj_loc, 1, pixel2d_gl::FALSE, proj.as_slice().as_ptr());
            (*context).UniformMatrix4fv(proj_loc, 1, pixel2d_gl::FALSE, projection.as_slice().as_ptr() as *const f32);
            (*context).UniformMatrix4fv(movw_loc, 1, pixel2d_gl::FALSE, (view * model).as_slice().as_ptr() as *const f32);
            (*context).Uniform1f(fade_loc, fade);

            (*context).DrawArrays(
                pixel2d_gl::TRIANGLES,
                0,
                3
            );
        }

        self.vao.unbind();
        self.program.unbind();

        unsafe {
            (*context).Disable(pixel2d_gl::DEPTH_TEST);
            (*context).Disable(pixel2d_gl::CULL_FACE);
            (*context).Disable(pixel2d_gl::BLEND);
        }

        Ok(())
    }
}

struct Window {
    width: i32,
    height: i32,

    tri: Triangle,
    bg: ColorBuffer,

    time: f32,

    camera: Camera,
}

impl Window {
    pub fn new(w: i32, h: i32) -> Self {
        let mut win: Window = Default::default();

        win.width  = w;
        win.height = h;

        win.camera.update_aspect(w as f32 / h as f32);

        win
    }

    // pub fn get_up(&self) -> Vector {
        // let dir: Vector   = self.camera - self.target;
        // let right: Vector = Vector::y() * dir;

        // dir * right
    // }
}

impl Default for Window {
    fn default() -> Self {
        Window {
            width: 640,
            height: 480,
            tri: Triangle::default(),
            bg: ColorBuffer::default(),
            time: 0.,
            camera: Camera::new(16./9., ::std::f32::consts::FRAC_PI_2, 0.01, 1000., 2.0),
        }
    }
}

impl PixelDrawer for Window {
    fn on_user_create(&mut self, _context: *const pixel2d_gl::Gl) -> bool {
        let res = match Resources::from_relative_exe_path(Path::new("assets")) {
            Ok(x) => x,
            Err(e) => {
                error!("Error while loading the assets: {}.", e);
                return false;
            },
        };

        match self.tri.load(&res) {
            Ok(_) => {},
            Err(e) => {
                error!("Error while loading the triangle: {}.", e);
                return false;
            },
        };

        self.bg = ColorBuffer::from_color(na::Vector3::new(0.3, 0.3, 0.5));
        self.bg.apply();

        true
    }

    fn on_user_update(&mut self, context: *const pixel2d_gl::Gl, evt: &Events, elapsed_time: f32) -> bool {
        self.time += elapsed_time;
        let angle = (self.time * 10.)  % 360.;

        // self.camera.zoom((self.time * 0.1) % 3. - 1.5);
        self.camera.rotate(&na::Vector3::z_axis(), angle);

        debug!("{}::Window::on_user_update - window size: {}x{}.", module_path!(), evt.width, evt.height);
        self.camera.update_aspect(evt.width as f32 / evt.height as f32);

        match self.tri.render(context, &self.camera.projection(), &self.camera.view2(), elapsed_time) {
            Err(e) => {
                error!("Unable to render triangle: {}.", e);
                false
            },
            Ok(_) => true,
        }
    }
}

pub fn main() -> Result<(), String> {
    let width = 640;
    let height = 480;

    if let Err(e) = stderrlog::new()
        .quiet(false)
        .verbosity(6)
        .timestamp(stderrlog::Timestamp::Millisecond)
        .modules(vec!["pixel_2d::math::matrix", "t3d"])
        .init() {
        return Err(format!("Error while creating the logger: {:?}.", e));
    }

    let mut app = Pixel2D::create()
        .title("Yipikay")
        .width(width)
        .height(height)
        // .resizable()
        .build(
            Box::new(
                Window::new(width, height)
            )
        )?;

    app.run();

    Ok(())
}
