use std::path::Path;

use pixel_2d::{Events, PixelDrawer, Pixel2D};
use pixel_2d::resources::Resources;
use pixel_2d::render::{Program, Vec3f32, u2_u10_u10_u10_rev_float, ColorBuffer};
use pixel_2d::render::buffer::{Buffer, VertexArray, BufferArray};

use pixel2d_derive::VertexAttribPointers;

use nalgebra as na;

use pixel2d_gl;

use log::error;

#[derive(Copy,Clone,Debug)]
#[derive(VertexAttribPointers)]
#[repr(C, packed)]
struct Vertex {
    #[location = 0]
    pos: Vec3f32,
    #[location = 1]
    clr: u2_u10_u10_u10_rev_float,
}

#[derive(Default)]
struct Window {
    shader: Program,

    vertices: Vec<Vertex>,

    vbo: Buffer<BufferArray>,
    vao: VertexArray,

    bg: ColorBuffer,

    res: Resources,
}

impl Window {
    fn load_shader(&mut self) -> bool {
        self.shader = match self.res.load_shader_program("triangle") {
            Err(e) => {
                error!("Unable to load the shader program 'triangle': '{}'.", e);
                return false;
            },
            Ok(s) => s,
        };

        true
    }
}

impl PixelDrawer for Window {
    fn on_user_create(&mut self, _context: *const pixel2d_gl::Gl) -> bool {
        self.bg = ColorBuffer::from_color(na::Vector3::new(0.3, 0.3, 0.5));

        self.res = match Resources::from_relative_exe_path(Path::new("assets")) {
            Ok(x) => x,
            Err(_) => return false,
        };

        self.bg.apply();

        if ! self.load_shader() {
            return false;
        }

        self.vertices = vec![
            Vertex { pos: (-0.5, -0.5, 0.0).into(), clr: (1.0, 0.0, 0.0, 1.0).into() },
            Vertex { pos: ( 0.5, -0.5, 0.0).into(), clr: (0.0, 1.0, 0.0, 1.0).into() },
            Vertex { pos: ( 0.0,  0.5, 0.0).into(), clr: (0.0, 0.0, 1.0, 1.0).into() },
        ];

        self.vbo = Buffer::new();
        self.vbo.bind();
        self.vbo.static_draw(&self.vertices);
        self.vbo.unbind();

        self.vao = VertexArray::new();
        self.vao.bind();
        self.vbo.bind();
        Vertex::vertex_attrib_pointers();
        self.vbo.unbind();
        self.vao.unbind();

        true
    }

    fn on_user_update(&mut self, context: *const pixel2d_gl::Gl, _: &Events, _: f32) -> bool {
        self.shader.bind();
        self.vao.bind();
        unsafe {
            (*context).DrawArrays(
                pixel2d_gl::TRIANGLES,
                0,
                3
            );
        }
        true
    }
}

pub fn main() -> Result<(), String> {
    if let Err(e) = stderrlog::new().quiet(false).verbosity(6).timestamp(stderrlog::Timestamp::Millisecond).init() {
        return Err(format!("Error while creating the logger: {:?}.", e));
    }

    let mut app = Pixel2D::create()
        .title("Yipikay")
        .width(640)
        .height(480)
        .resizable()
        .build(
            Box::new(
                Window::default()
            )
        )?;

    app.run();

    Ok(())
}
