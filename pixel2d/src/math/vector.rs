use std::fmt::{Display, Error as FmtError, Formatter};

use std::ops::{Add, AddAssign};
use std::ops::{Div, DivAssign};
use std::ops::{Mul, MulAssign};
use std::ops::Neg;
use std::ops::{Sub, SubAssign};

use std::ops::{Index, IndexMut};

#[derive(Copy,Clone,Debug)]
pub struct Vector(f32, f32, f32);

impl Vector {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Vector(x, y, z)
    }

    pub fn zero() -> Self {
        Vector(0., 0., 0.)
    }

    pub fn x() -> Self {
        Vector(1., 0., 0.)
    }

    pub fn y() -> Self {
        Vector(0., 1., 0.)
    }

    pub fn z() -> Self {
        Vector(0., 0., 1.)
    }

    pub fn norm(&self) -> f32 {
        (self.0 * self.0 + self.1 * self.1 + self.2 * self.2).sqrt()
    }
}

impl Sub for Vector {
    type Output = Vector;

    fn sub(self, right: Vector) -> Self {
        Vector(self.0 - right.0, self.1 - right.1, self.2 - right.2)
    }
}

impl SubAssign for Vector {
    fn sub_assign(&mut self, right: Vector) {
        self.0 -= right.0;
        self.1 -= right.1;
        self.2 -= right.2;
    }
}

impl Add for Vector {
    type Output = Vector;

    fn add(self, right: Vector) -> Self {
        Vector(self.0 + right.0, self.1 + right.1, self.2 + right.2)
    }
}

impl AddAssign for Vector {
    fn add_assign(&mut self, right: Vector) {
        self.0 -= right.0;
        self.1 -= right.1;
        self.2 -= right.2;
    }
}

impl Mul for Vector {
    type Output = Vector;

    fn mul(self, right: Vector) -> Self {
        Vector(
            self.1 * right.2 - self.2 * right.1,
            self.2 * right.0 - self.0 * right.2,
            self.0 * right.1 - self.1 * right.0
        )
    }
}

impl MulAssign for Vector {
    fn mul_assign(&mut self, right: Vector) {
        self.0 = self.1 * right.2 - self.2 * right.1;
        self.1 = self.2 * right.0 - self.0 * right.2;
        self.2 = self.0 * right.1 - self.1 * right.0;
    }
}

impl Mul<f32> for Vector {
    type Output = Vector;

    fn mul(self, right: f32) -> Self {
        Vector(
            self.1 * right,
            self.2 * right,
            self.0 * right
        )
    }
}

impl MulAssign<f32> for Vector {
    fn mul_assign(&mut self, right: f32) {
        self.0 *= right;
        self.1 *= right;
        self.2 *= right;
    }
}

impl Mul<Vector> for f32 {
    type Output = Vector;

    fn mul(self, right: Vector) -> Self::Output {
        Vector(
            right.1 * self,
            right.2 * self,
            right.0 * self
        )
    }
}

impl Div<f32> for Vector {
    type Output = Vector;

    fn div(self, right: f32) -> Self::Output {
        Vector(
            self.1 / right,
            self.2 / right,
            self.0 / right
        )
    }
}

impl DivAssign<f32> for Vector {
    fn div_assign(&mut self, right: f32) {
        self.0 /= right;
        self.1 /= right;
        self.2 /= right;
    }
}

impl Neg for Vector {
    type Output = Vector;

    fn neg(self) -> Self::Output {
        Vector(-self.0, -self.1, -self.2)
    }
}

impl Index<usize> for Vector {
    type Output = f32;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            2 => &self.2,
            _ => unreachable!(),
        }
    }
}

impl IndexMut<usize> for Vector {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            2 => &mut self.2,
            _ => unreachable!(),
        }
    }
}

impl Display for Vector {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        write!(f, "[{}, {}, {}]", self.0, self.1, self.2)
    }
}
