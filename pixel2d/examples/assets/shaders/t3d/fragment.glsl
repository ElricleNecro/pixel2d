#version 330 core

in VS_OUTPUT {
	vec3 Color;
} IN;

out vec4 Color;

uniform float fade;

void main()
{
    Color = vec4(IN.Color, fade);
}
