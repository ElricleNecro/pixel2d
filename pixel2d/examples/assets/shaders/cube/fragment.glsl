#version 330 core

in vec4 outcolor;
out vec4 Color;

void main()
{
    Color = outcolor;
}
