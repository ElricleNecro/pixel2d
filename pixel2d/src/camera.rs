use nalgebra as na;

pub struct Camera {
    target: na::Point3<f32>,
    position: na::Point3<f32>,
    rotation: na::UnitQuaternion<f32>,
    projection: na::Perspective3<f32>,
    distance: f32,
}

impl Camera {
    pub fn new(aspect: f32, fov: f32, znear: f32, zfar: f32, distance: f32) -> Self {
        Camera {
            target: na::Point3::origin(),

            position: na::Point3::new(0., 0., distance),

            rotation: na::UnitQuaternion::from_axis_angle(&na::Vector3::x_axis(), ::std::f32::consts::PI / 4.0),
            projection: na::Perspective3::new(aspect, fov, znear, zfar),

            distance,
        }
    }

    pub fn set_position(&mut self, pos: na::Point3<f32>) {
        self.position = pos;
    }

    pub fn get_up(&self) -> na::Vector3<f32> {
        let dir: na::Vector3<f32>   = self.position - self.target;
        let right: na::Vector3<f32> = na::Vector3::y().cross(&dir);

        dir.cross(&right)
    }

    pub fn view(&self) -> na::Matrix4<f32> {
        (na::Translation3::<f32>::from(self.target.coords)
            * self.rotation
            * na::Translation3::<f32>::from(na::Vector3::z() * self.distance)).inverse()
        .to_homogeneous()
    }

    pub fn view2(&self) -> na::Matrix4<f32> {
        na::Matrix4::look_at_rh(&self.position, &self.target, &self.get_up())
        // na::Matrix4::face_towards(&self.position, &self.target, &self.get_up())
    }

    pub fn projection(&self) -> na::Matrix4<f32> {
        self.projection.into_inner()
    }

    pub fn view_projection(&self) -> na::Matrix4<f32> {
        self.projection.into_inner() * self.view()
    }

    pub fn position(&self) -> na::Point3<f32> {
        na::Translation3::<f32>::from(self.target.coords)
            * self.rotation
            * na::Translation3::<f32>::from(na::Vector3::z() * self.distance)
            * na::Point3::<f32>::origin()
    }

    pub fn zoom(&mut self, z: f32) {
        self.distance -= z * self.speed();
    }

    fn speed(&self) -> f32 {
        let min_speed = 0.1;
        let max_speed = 20.0;
        let min_distance = 1.0;
        let max_distance = 500.0;

        if self.distance > max_distance {
            max_speed
        } else if self.distance < min_distance {
            min_speed
        } else {
            (self.distance - min_distance) / (max_distance - min_distance) * (max_speed - min_speed)
                + min_speed
        }
    }

    pub fn update_aspect(&mut self, aspect: f32) {
        self.projection.set_aspect(aspect);
    }

    pub fn rotate(&mut self, axes: &na::Unit<na::Vector3<f32>>, angle: f32) {
        self.rotation *= na::UnitQuaternion::from_axis_angle(axes, angle);
    }
}
