use std::ffi::CString;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;
use std::time::{Duration, SystemTime};

use pixel2d_gl;
use pixel2d_gl::{VERTEX_SHADER, FRAGMENT_SHADER};
use pixel2d_gl::types::GLint;

use log::{debug, info, warn};

use nalgebra as na;

use pixel2d_derive::VertexAttribPointers;

use sdl2::init;
use sdl2::event::Event;

static mut GL: *mut pixel2d_gl::Gl = std::ptr::null_mut();

#[derive(Copy,Clone,Debug)]
#[repr(C, packed)]
pub struct Vec3f32(f32, f32, f32);

impl Vec3f32 {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Vec3f32(x, y, z)
    }
}

impl From<(f32, f32, f32)> for Vec3f32 {
    fn from(t: (f32, f32, f32)) -> Self {
        Vec3f32(t.0, t.1, t.2)
    }
}

impl From<[f32; 3]> for Vec3f32 {
    fn from(t: [f32; 3]) -> Self {
        Vec3f32(t[0], t[1], t[2])
    }
}

impl Vec3f32 {
    pub fn vertex_attrib_pointer(stride: usize, location: usize, offset: usize) {
        unsafe {
            (*GL).EnableVertexAttribArray(location as pixel2d_gl::types::GLuint); // this is "layout (location = 0)" in vertex shader
            (*GL).VertexAttribPointer(
                location as pixel2d_gl::types::GLuint, // index of the generic vertex attribute ("layout (location = 0)")
                3, // the number of components per generic vertex attribute
                pixel2d_gl::FLOAT, // data type
                pixel2d_gl::FALSE, // normalized (int-to-float conversion)
                stride as pixel2d_gl::types::GLint, // stride (byte offset between consecutive attributes)
                offset as *const pixel2d_gl::types::GLvoid // offset of the first component
            );
        }
    }
}

#[derive(Copy, Clone, Debug)]
#[repr(C, packed)]
pub struct u2_u10_u10_u10_rev_float {
    pub inner: ::vec_2_10_10_10::Vector,
}

impl u2_u10_u10_u10_rev_float {
    pub fn new(inner: ::vec_2_10_10_10::Vector) -> u2_u10_u10_u10_rev_float {
        u2_u10_u10_u10_rev_float { inner }
    }

    pub  fn vertex_attrib_pointer(
        stride: usize,
        location: usize,
        offset: usize,
    ) {
        unsafe {        (*GL).EnableVertexAttribArray(location as pixel2d_gl::types::GLuint);
        (*GL).VertexAttribPointer(
            location as pixel2d_gl::types::GLuint,
            4, // the number of components per generic vertex attribute
            pixel2d_gl::UNSIGNED_INT_2_10_10_10_REV, // data type
            pixel2d_gl::TRUE, // normalized (int-to-float conversion)
            stride as pixel2d_gl::types::GLint,
            offset as *const pixel2d_gl::types::GLvoid,
);
}
    }
}

impl From<(f32, f32, f32, f32)> for u2_u10_u10_u10_rev_float {
    fn from(other: (f32, f32, f32, f32)) -> Self {
        u2_u10_u10_u10_rev_float {
            inner: ::vec_2_10_10_10::Vector::new(other.0, other.1, other.2, other.3),
        }
    }
}

pub trait BufferType {
    const BUFFER_TYPE: pixel2d_gl::types::GLuint;
}

#[derive(Default)]
pub struct BufferArray;
impl BufferType for BufferArray {
    const BUFFER_TYPE: pixel2d_gl::types::GLuint = pixel2d_gl::ARRAY_BUFFER;
}

#[derive(Default)]
pub struct ElementArray;
impl BufferType for ElementArray {
    const BUFFER_TYPE: pixel2d_gl::types::GLuint = pixel2d_gl::ELEMENT_ARRAY_BUFFER;
}

/// Represent array buffer (VBO: virtual buffer object).
#[derive(Default)]
pub struct Buffer<B> where B: BufferType {
    vbo: pixel2d_gl::types::GLuint,
    _marker: ::std::marker::PhantomData<B>,
}

impl<B> Buffer<B> where B: BufferType {
    /// Create a new virtual buffer object (array buffer).
    pub fn new() -> Self {
        let mut vbo: pixel2d_gl::types::GLuint = 0;

        unsafe {
            (*GL).GenBuffers(1, &mut vbo);
        }

        Buffer {
            vbo,
            _marker: ::std::marker::PhantomData,
        }
    }

    /// Bind the array buffer.
    pub fn bind(&self) {
        unsafe {
            (*GL).BindBuffer(B::BUFFER_TYPE, self.vbo);
        }
    }

    /// Unbind the array buffer.
    pub fn unbind(&self) {
        unsafe {
            (*GL).BindBuffer(B::BUFFER_TYPE, 0);
        }
    }

    /// Attach the given array to the VBO using the STATIC_DRAW method.
    pub fn static_draw<T>(&self, data: &[T]) {
        unsafe {
            (*GL).BufferData(
                B::BUFFER_TYPE,                                                     // Target
                (data.len() * ::std::mem::size_of::<T>()) as pixel2d_gl::types::GLsizeiptr, // Size of data in bytes
                data.as_ptr() as *const pixel2d_gl::types::GLvoid,                          // Pointer to data
                pixel2d_gl::STATIC_DRAW,                                                    // Usage
            );
        }
    }
}

impl<B> Drop for Buffer<B> where B: BufferType {
    fn drop(&mut self) {
        if self.vbo == 0 {
            return ;
        }

        unsafe {
            (*GL).DeleteBuffers(1, &self.vbo);
        }
    }
}

/// Shortcut to create an array buffer.
pub type ArrayBuffer = Buffer<BufferArray>;
/// Shortcut to create an element array buffer.
pub type ElementArrayBuffer = Buffer<ElementArray>;

#[derive(Default)]
pub struct VertexArray(pixel2d_gl::types::GLuint);

impl VertexArray {
    pub fn new() -> Self {
        let mut vao: pixel2d_gl::types::GLuint = 0;

        unsafe {
            (*GL).GenVertexArrays(1, &mut vao);
        }

        VertexArray(vao)
    }

    pub fn bind(&self) {
        unsafe {
            (*GL).BindVertexArray(self.0);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            (*GL).BindVertexArray(0);
        }
    }
}

impl Drop for VertexArray {
    fn drop(&mut self) {
        if self.0 == 0 {
            return ;
        }

        unsafe {
            (*GL).DeleteVertexArrays(1, &self.0);
        }
    }
}

#[derive(Copy,Clone,Debug)]
#[derive(VertexAttribPointers)]
#[repr(C, packed)]
struct Vertex {
    #[location = 0]
    pos: Vec3f32,
    #[location = 1]
    clr: u2_u10_u10_u10_rev_float,
}

pub fn get<P: AsRef<Path>>(name: P) -> Result<String, String> {
    let file = name.as_ref().to_path_buf();
    info!("Loading file '{}'.", file.display());

    let mut reader = BufReader::new(File::open(file).map_err(|e| e.to_string())?);
    let mut buffer = String::new();

    reader.read_to_string(&mut buffer).map_err(|e| e.to_string())?;

    Ok(buffer)
}

fn empty_cstring(len: usize) -> CString {
    // Allocating the vector:
    let mut buffer: Vec<u8> = Vec::with_capacity(len + 1);
    // Filling it with whitespace characters:
    buffer.extend([b' '].iter().cycle().take(len));
    // Returning a new CString:
    unsafe {
        CString::from_vec_unchecked(buffer)
    }
}

pub fn main() -> Result<(), String> {
    let sdl2 = init()?;
    let video = sdl2.video()?;
    let gl_attr = video.gl_attr();

    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(4, 3);

    let mut w_builder = &mut video.window("Testy", 640, 480);
    w_builder = w_builder.position_centered();
    w_builder = w_builder.opengl();

    let window = match w_builder.build() {
        Ok(o) => o,
        Err(e) => { return Err(e.to_string()); },
    };

    let mut event = sdl2.event_pump()?;
    let _context = window.gl_create_context();

    unsafe {
        GL = std::mem::transmute(Box::new(
            pixel2d_gl::Gl::load_with(|s| video.gl_get_proc_address(s) as *const std::os::raw::c_void)
        ));

        if GL.is_null() {
            return Err("Unable to initialise GL component.".to_string());
        }
    }

    unsafe {
        (*GL).Viewport(0, 0, 640, 480);
    }

    unsafe {
        (*GL).ClearColor(0.0, 0.0, 0.0, 0.0);
        // (*GL).ClearColor(0.3, 0.3, 0.5, 1.0);
    }

    let vertex_shader_id = unsafe {
        (*GL).CreateShader(VERTEX_SHADER)
    };
    let c_source = match CString::new(get("examples/assets/shaders/t3d/vertex.glsl")?) {
        Ok(s)  => s,
        Err(e) => { return Err(e.to_string()) },
    };
    let mut success: GLint = 1;

    // All GL function are unsafe because automatically generated from C:
    unsafe {
        // Attach the source to the newly created shader:
        (*GL).ShaderSource(vertex_shader_id, 1, &c_source.as_ptr(), std::ptr::null());
        // Compiling it:
        (*GL).CompileShader(vertex_shader_id);
        // Checking for errors:
        (*GL).GetShaderiv(vertex_shader_id, pixel2d_gl::COMPILE_STATUS, &mut success);
    }

    // Has there been any kind of error?
    if success == 0 {
        // There was an error, extracting it.
        let mut len: GLint = 0;
        unsafe {
            // Getting the length of the info log:
            (*GL).GetShaderiv(vertex_shader_id, pixel2d_gl::INFO_LOG_LENGTH, &mut len);
        }
        // Allocating an empty buffer with this size:
        let error = empty_cstring(len as usize);
        unsafe {
            // Using this buffer to extract the error message:
            (*GL).GetShaderInfoLog(
                vertex_shader_id,
                len,
                std::ptr::null_mut(),
                error.as_ptr() as *mut pixel2d_gl::types::GLchar
            );
        }

        // Returning an error:
        return Err(error.to_string_lossy().into_owned());
    }
    let fragment_shader_id = unsafe {
        (*GL).CreateShader(FRAGMENT_SHADER)
    };
    let c_source = match CString::new(get("examples/assets/shaders/t3d/fragment.glsl")?) {
        Ok(s)  => s,
        Err(e) => { return Err(e.to_string()) },
    };
    let mut success: GLint = 1;

    // All GL function are unsafe because automatically generated from C:
    unsafe {
        // Attach the source to the newly created shader:
        (*GL).ShaderSource(fragment_shader_id, 1, &c_source.as_ptr(), std::ptr::null());
        // Compiling it:
        (*GL).CompileShader(fragment_shader_id);
        // Checking for errors:
        (*GL).GetShaderiv(fragment_shader_id, pixel2d_gl::COMPILE_STATUS, &mut success);
    }

    // Has there been any kind of error?
    if success == 0 {
        // There was an error, extracting it.
        let mut len: GLint = 0;
        unsafe {
            // Getting the length of the info log:
            (*GL).GetShaderiv(fragment_shader_id, pixel2d_gl::INFO_LOG_LENGTH, &mut len);
        }
        // Allocating an empty buffer with this size:
        let error = empty_cstring(len as usize);
        unsafe {
            // Using this buffer to extract the error message:
            (*GL).GetShaderInfoLog(
                fragment_shader_id,
                len,
                std::ptr::null_mut(),
                error.as_ptr() as *mut pixel2d_gl::types::GLchar
            );
        }

        // Returning an error:
        return Err(error.to_string_lossy().into_owned());
    }
    let program = unsafe {
        (*GL).CreateProgram()
    };
    unsafe {
        (*GL).AttachShader(program, vertex_shader_id);
        (*GL).AttachShader(program, fragment_shader_id);
    }
    unsafe {
        (*GL).LinkProgram(program);
    }

    // Will be used for error handling:
    let mut success: GLint = 1;

    unsafe {
        // Checking for errors:
        (*GL).GetProgramiv(program, pixel2d_gl::LINK_STATUS, &mut success);
    }

    // Has there been any kind of error?
    if success == 0 {
        // There was an error, extracting it.
        let mut len: GLint = 0;
        unsafe {
            // Getting the length of the info log:
            (*GL).GetProgramiv(program, pixel2d_gl::INFO_LOG_LENGTH, &mut len);
        }
        // Allocating an empty buffer with this size:
        let error = empty_cstring(len as usize);
        unsafe {
            // Using this buffer to extract the error message:
            (*GL).GetProgramInfoLog(
                program,
                len,
                std::ptr::null_mut(),
                error.as_ptr() as *mut pixel2d_gl::types::GLchar
            );
        }

        // Returning an error:
        return Err(error.to_string_lossy().into_owned());
    }
    unsafe {
        (*GL).DetachShader(program, vertex_shader_id);
        (*GL).DetachShader(program, fragment_shader_id);
    }

    let vertices = vec![
        Vertex { pos: (-0.5, -0.5, 0.0).into(), clr: (1.0, 0.0, 0.0, 1.0).into() },
        Vertex { pos: ( 0.5, -0.5, 0.0).into(), clr: (0.0, 1.0, 0.0, 1.0).into() },
        Vertex { pos: ( 0.0,  0.5, 0.0).into(), clr: (0.0, 0.0, 1.0, 1.0).into() },
    ];

    let vbo = ArrayBuffer::new();
    vbo.bind();
    vbo.static_draw(&vertices);
    vbo.unbind();

    let vao = VertexArray::new();
    vao.bind();
    vbo.bind();
    Vertex::vertex_attrib_pointers();
    vbo.unbind();
    vao.unbind();

    let framerate = (1000. / 30.) as u64;
    let fps = Duration::from_millis(framerate);

    let projection = na::Matrix4::new_perspective(640./480., 120., 0.0001, 1000.);
    let view = na::Matrix4::<f32>::face_towards(
        &na::Point3::new(5., 5., 5.),
        &na::Point3::new(0., 0., 0.),
        &na::Vector3::new(0., 1., 0.),
    );

    'll: loop {
        let time = SystemTime::now();

        for evt in event.poll_iter() {
            match evt {
                Event::Quit { .. } => break 'll,
                Event::Window {
                    win_event: sdl2::event::WindowEvent::Resized(w, h),
                    ..
                } => unsafe {
                    (*GL).Viewport(0, 0, w, h);
                },
                _           => {},
            }
        }

        unsafe {
            (*GL).Clear(pixel2d_gl::COLOR_BUFFER_BIT | pixel2d_gl::DEPTH_BUFFER_BIT);
        }

        unsafe {
            (*GL).UseProgram(program);
        }
        vao.bind();

        let c_source = match CString::new("modelview") {
            Ok(s)  => s,
            Err(e) => { return Err(e.to_string()) },
        };
        let proj_loc = unsafe {
            (*GL).GetUniformLocation(program, c_source.as_ptr())
        };
        let c_source = match CString::new("projection") {
            Ok(s)  => s,
            Err(e) => { return Err(e.to_string()) },
        };
        let movw_loc = unsafe {
            (*GL).GetUniformLocation(program, c_source.as_ptr())
        };
        debug!("proj loc: {}.", proj_loc);
        debug!("mvow loc: {}.", movw_loc);

        unsafe {
            // (*GL).UniformMatrix4fv(proj_loc, 1, pixel2d_gl::FALSE, (projection * view).to_homogeneous().as_slice().as_ptr());
            (*GL).UniformMatrix4fv(proj_loc, 1, pixel2d_gl::FALSE, projection.as_slice().as_ptr());
            (*GL).UniformMatrix4fv(movw_loc, 1, pixel2d_gl::FALSE, (view).to_homogeneous().as_slice().as_ptr());

            (*GL).DrawArrays(
                pixel2d_gl::TRIANGLES,
                0,
                3
            );
        }

        vao.unbind();
        unsafe {
            (*GL).UseProgram(0);
        }

        window.gl_swap_window();

        let elapsed = match time.elapsed() {
            Ok(v) => v,
            Err(e) => {
                warn!("Error while getting elapsed time: {}", e);
                continue;
            }
        };

        if elapsed < fps {
            std::thread::sleep(fps - elapsed);
        }
    }

    Ok(())
}
