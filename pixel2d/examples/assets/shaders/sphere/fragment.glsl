#version 330 core

in  vec4 loccolor;
out vec4 vfragColor;

void main()
{
    vfragColor = loccolor;
}
