use nalgebra as na;
use pixel2d_gl;

use crate::GL;

pub struct ColorBuffer {
    pub color: na::Vector4<f32>,
}

impl ColorBuffer {
    pub fn from_color(color: na::Vector3<f32>) -> Self {
        ColorBuffer {
            color: color.fixed_resize::<na::U4, na::U1>(1.0),
        }
    }

    pub fn update(&mut self, color: na::Vector3<f32>) {
        self.color = color.fixed_resize::<na::U4, na::U1>(1.0);
    }

    pub fn apply(&self) {
        unsafe {
            (*GL).ClearColor(self.color.x, self.color.y, self.color.z, 1.0);
        }
    }

    pub fn clear(&self) {
        unsafe {
            (*GL).Clear(pixel2d_gl::COLOR_BUFFER_BIT);
        }
    }
}

impl Default for ColorBuffer {
    fn default() -> Self {
        ColorBuffer {
            color: na::Vector4::new(0., 0., 0., 0.),
        }
    }
}
