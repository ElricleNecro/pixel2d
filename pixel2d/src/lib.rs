use std::convert::TryInto;
use std::time::{Duration, SystemTime};

use sdl2::init;
use sdl2::video::{GLContext, Window};

use pixel2d_gl;

use log::warn;

mod camera;
pub mod events;
pub mod math;
pub mod render;
pub mod resources;

pub use events::Events;
pub use camera::Camera;

pub(crate) static mut GL: *mut pixel2d_gl::Gl = std::ptr::null_mut();

pub trait PixelDrawer {
    fn on_user_create(&mut self, gl: *const pixel2d_gl::Gl) -> bool;
    fn on_user_update(&mut self, gl: *const pixel2d_gl::Gl, evt: &Events, elapsed_time: f32) -> bool;

    fn on_user_destroy(&mut self) -> bool {
        true
    }
}

pub struct Pixel2D {
    event: Events,
    context: GLContext,

    window: Window,

    framerate: u64,

    drawer: Box<dyn PixelDrawer>,

    width: i32,
    height: i32,
}

#[derive(Default)]
pub struct Pixel2DBuilder {
    title: String,

    width: i32,
    height: i32,

    framerate: u64,

    resizable: bool,
    fullscreen: bool,
    borderless: bool,
}

impl Pixel2DBuilder {
    #[inline]
    pub fn title(&mut self, title: &str) -> &mut Self {
        self.title = title.to_owned();
        self
    }

    #[inline]
    pub fn width(&mut self, width: i32) -> &mut Self {
        self.width = width;
        self
    }

    #[inline]
    pub fn height(&mut self, height: i32) -> &mut Self {
        self.height = height;
        self
    }

    #[inline]
    pub fn resizable(&mut self) -> &mut Self {
        self.resizable = true;
        self
    }

    #[inline]
    pub fn borderless(&mut self) -> &mut Self {
        self.borderless = true;
        self
    }

    #[inline]
    pub fn fullscreen(&mut self) -> &mut Self {
        self.fullscreen = true;
        self
    }

    #[inline]
    pub fn fps(&mut self, t: f64) -> &mut Self {
        self.framerate = (1000. / t) as u64;
        self
    }

    pub fn build(&self, layer: Box<dyn PixelDrawer>) -> Result<Pixel2D, String> {
        let sdl2 = init()?;
        let video = sdl2.video()?;
        let gl_attr = video.gl_attr();

        gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
        gl_attr.set_context_version(4, 3);
        gl_attr.set_alpha_size(1);

        let mut w_builder = &mut video.window(
            &self.title,
            match self.width.try_into() {
                Ok(x) => x,
                Err(e) => return Err(format!("Error: {}.", e)),
            },
            match self.height.try_into() {
                Ok(x) => x,
                Err(e) => return Err(format!("Error: {}.", e)),
            }
        );
        w_builder = w_builder.position_centered();
        w_builder = w_builder.opengl();

        if self.resizable {
            w_builder = w_builder.resizable();
        }

        if self.fullscreen {
            w_builder = w_builder.fullscreen();
        }

        if self.borderless {
            w_builder = w_builder.borderless();
        }

        let win = match w_builder.build() {
            Ok(o) => o,
            Err(e) => { return Err(e.to_string()); },
        };

        let engine = Pixel2D {
            event: Events::new(
                sdl2.event_pump()?,
                self.width,
                self.height
            ),
            context: win.gl_create_context()?,
            window: win,

            framerate: if self.framerate == 0 { (1000. / 30.) as u64 } else { self.framerate },

            drawer: layer,

            width: self.width,
            height: self.height,
        };

        // Preparing the Opengl library pipeline:
        unsafe {
            GL = std::mem::transmute(Box::new(
                pixel2d_gl::Gl::load_with(|s| video.gl_get_proc_address(s) as *const std::os::raw::c_void)
            ));

            if GL.is_null() {
                return Err("Unable to initialise GL component.".to_string());
            }
        }

        Ok(engine)
    }
}

impl Pixel2D {
    #[inline]
    pub fn create() -> Pixel2DBuilder {
        Pixel2DBuilder::default()
    }

    pub fn run(&mut self) {
        let gl_instance: *const pixel2d_gl::Gl = unsafe {
            GL
        };

        let mut viewport = render::Viewport::for_window(self.width as i32, self.height as i32);
        viewport.apply();

        if ! self.drawer.on_user_create(gl_instance) {
            return ;
        }

        let mut elapsed_time: f32 = 0.;
        let fps = Duration::from_millis(self.framerate);

        'main: loop {
            let time = SystemTime::now();

            self.event.update();
            viewport.update(self.event.width, self.event.height);
            if self.event.quit {
                break 'main;
            }

            unsafe {
                (*GL).Clear(pixel2d_gl::COLOR_BUFFER_BIT | pixel2d_gl::DEPTH_BUFFER_BIT);
            }

            if ! self.drawer.on_user_update(gl_instance, &self.event, elapsed_time) {
                break 'main;
            }

            self.window.gl_swap_window();

            let elapsed = match time.elapsed() {
                Ok(v) => v,
                Err(e) => {
                    warn!("{} - Error while getting elapsed time: {}", module_path!(), e);
                    continue;
                }
            };

            if elapsed < fps {
                std::thread::sleep(fps - elapsed);
            }

            elapsed_time = match time.elapsed() {
                Ok(v) => (v.as_secs() * 1_000_000_000 + u64::from(v.subsec_nanos())) as f32 / 1e9,
                Err(e) => {
                    warn!("Error while getting elapsed time: {}", e);
                    continue;
                }
            };
        }

        self.drawer.on_user_destroy();
    }
}
