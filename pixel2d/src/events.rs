use sdl2::EventPump;
use sdl2::event::{Event, WindowEvent};
pub use sdl2::keyboard::Scancode;
pub use sdl2::mouse::MouseButton;

use std::ops::Index;

/// Represent keyboard events:
pub struct Events {
    pump: EventPump,

    key: [bool; 512],
    mouse: [bool; 6],

    /// Quit event has occured.
    pub quit: bool,
    /// Window lost focus.
    pub focus: bool,

    /// Mouse x position.
    pub x: i32,
    /// Mouse y position.
    pub y: i32,
    /// Mouse x relative position.
    pub x_rel: i32,
    /// Mouse y relative position.
    pub y_rel: i32,

    /// Window width.
    pub width: i32,
    /// Window height.
    pub height: i32,
}

impl Events {
    pub(crate) fn new(evt: EventPump, width: i32, height: i32) -> Self {
        Self {
            pump: evt,

            key: [false; 512],
            mouse: [false; 6],

            quit: false,
            focus: true,

            x: 0,
            y: 0,
            x_rel: 0,
            y_rel: 0,

            width,
            height,
        }
    }

    pub(crate) fn update(&mut self) {
        self.x_rel = 0;
        self.y_rel = 0;

        // Finding out which keys where pressed/released or what other kind of supported event
        // happened:
        for event in self.pump.poll_iter() {
            match event {
                Event::Quit            { .. }                                            => self.quit = true,

                Event::KeyDown         { scancode: Some(x), .. }                         => self.key[x as usize] = true,
                Event::KeyUp           { scancode: Some(x), .. }                         => self.key[x as usize] = false,

                Event::MouseButtonDown { mouse_btn: MouseButton::Left, .. }              => self.mouse[0] = true,
                Event::MouseButtonDown { mouse_btn: MouseButton::Right, .. }             => self.mouse[1] = true,
                Event::MouseButtonDown { mouse_btn: MouseButton::Middle, .. }            => self.mouse[2] = true,
                Event::MouseButtonDown { mouse_btn: MouseButton::X1, .. }                => self.mouse[3] = true,
                Event::MouseButtonDown { mouse_btn: MouseButton::X2, .. }                => self.mouse[4] = true,

                Event::MouseButtonUp   { mouse_btn: MouseButton::Left, .. }              => self.mouse[0] = false,
                Event::MouseButtonUp   { mouse_btn: MouseButton::Right, .. }             => self.mouse[1] = false,
                Event::MouseButtonUp   { mouse_btn: MouseButton::Middle, .. }            => self.mouse[2] = false,
                Event::MouseButtonUp   { mouse_btn: MouseButton::X1, .. }                => self.mouse[3] = false,
                Event::MouseButtonUp   { mouse_btn: MouseButton::X2, .. }                => self.mouse[4] = false,

                Event::MouseMotion     { x, y, xrel, yrel, .. }                          => {
                    self.x = x;
                    self.y = y;
                    self.x_rel = xrel;
                    self.y_rel = yrel;
                },

                Event::Window          { win_event: WindowEvent::FocusLost, .. }         => self.focus = false,
                Event::Window          { win_event: WindowEvent::FocusGained, .. }       => self.focus = true,

                Event::Window          { win_event: WindowEvent::Resized(x, y), .. }     => {
                    self.width  = x;
                    self.height = y;
                },
                Event::Window          { win_event: WindowEvent::SizeChanged(x, y), .. } => {
                    self.width  = x;
                    self.height = y;
                },

                _ => {},
            }
        }
    }
}

impl Index<MouseButton> for Events {
    type Output = bool;

    fn index(&self, btn: MouseButton) -> &Self::Output {
        &self.mouse[btn as usize]
    }
}

impl Index<Scancode> for Events {
    type Output = bool;

    fn index(&self, key: Scancode) -> &Self::Output {
        &self.key[key as usize]
    }
}
