use std::io::{BufReader, Read};
use std::fs::File;
use std::fmt::{Display, Formatter, self};
use std::path::{Path, PathBuf};

use log::info;

use crate::render::shader::{Shader, ShaderError, Program};
use crate::render::Vec3f32;

use pixel2d_derive::VertexAttribPointers;

#[derive(Debug)]
pub enum Error {
    FileNotFound { file: PathBuf, err: std::io::Error },
    Io(std::io::Error),
    ExePathNotFound(std::io::Error),
    NoParentDir,
    Shader(ShaderError),
}

impl From<std::io::Error> for Error {
    fn from(other: std::io::Error) -> Self {
        Error::Io(other)
    }
}

impl From<ShaderError> for Error {
    fn from(err: ShaderError) -> Self {
        Error::Shader(err)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Error::FileNotFound {file, err} => write!(f, "File '{}' not found ({})", file.display(), err),
            Error::Io(e) => write!(f, "Some error happened when trying to read a file: '{}'", e),
            Error::ExePathNotFound(e) => write!(f, "Unable to find executable location: '{:?}'", e),
            Error::NoParentDir => write!(f, "No parent directory, this should not happened."),
            Error::Shader(s) => write!(f, "{}", s),
        }
    }
}

pub(crate) fn name_to_path(root: &Path, loc: &str) -> PathBuf {
    let mut path: PathBuf = root.into();

    for chunk in loc.split('/') {
        path = path.join(chunk);
    }

    path
}

// pub(crate) fn str_to_path(loc: &str) -> PathBuf {
    // let mut path: PathBuf = PathBuf::new();

    // for chunk in loc.split('/') {
        // path = path.join(chunk);
    // }

    // path
// }

// pub(crate) fn concat_path(way: &[&Path]) -> PathBuf {
    // let mut path: PathBuf = PathBuf::new();

    // for chunk in way {
        // path = path.join(chunk);
    // }

    // path
// }

#[derive(Copy,Clone,Debug)]
#[derive(VertexAttribPointers)]
#[repr(C, packed)]
struct Vertex {
    #[location = 0]
    pos: Vec3f32,
    #[location = 1]
    normal: Vec3f32,
    #[location = 2]
    texcoord: Vec3f32,
}

// struct Mesh {
    // data: Vec<Vertex>,
    // indices: Vec<u32>,
// }

#[derive(Default)]
pub struct Resources {
    root: PathBuf,
}

impl Resources {
    pub fn from_relative_exe_path(path: &Path) -> Result<Self, Error> {
        let root = std::env::current_exe()
            .map_err(Error::ExePathNotFound)?
            .parent()
            .ok_or(Error::NoParentDir)?
            .join(path);

        info!("{} - Root of resources is: '{}'.", module_path!(), root.display());

        Ok(Self {
            root,
        })
    }

    pub fn get<P: AsRef<Path>>(&self, name: P) -> Result<String, Error> {
        let fname = name.as_ref().to_path_buf();
        let file = self.root.join(name);
        info!("{} - Loading file '{}'.", module_path!(), file.display());

        let mut reader = BufReader::new(File::open(file).map_err(|e| Error::FileNotFound {file: fname, err: e})?);
        let mut buffer = String::new();

        reader.read_to_string(&mut buffer)?;

        Ok(buffer)
    }

    pub fn load_shader_program(&self, prg: &str) -> Result<Program, Error> {
        let path = name_to_path(Path::new("shaders"), prg);

        let mut shaders = Vec::new();
        // let vertex   = Shader::new_vertex(&self.get(path.join("vertex.glsl"))?)?;
        // let fragment = Shader::new_fragment(&self.get(path.join("fragment.glsl"))?)?;
        shaders.push(Shader::new_vertex(&self.get(path.join("vertex.glsl"))?)?);
        shaders.push(Shader::new_fragment(&self.get(path.join("fragment.glsl"))?)?);

        Program::new_from_shaders(&shaders).map_err(|e| e.into())
    }

    // pub fn load_obj(&self, name: &str) -> Result<(), Error> {
        // let path = concat_path(&[&self.root, Path::new("models"), &str_to_path(name)]);
        // let tmp  = path.to_path_buf();
        // let reader = BufReader::new(File::open(path).map_err(|e| Error::FileNotFound {file: tmp, err: e})?);
        // let reader = reader.lines()
            // .filter(|x| x.is_ok())
            // .map(|x| x.unwrap())
            // .map(|x| x.trim().to_string())
            // ;

        // let mut vertex: Vec<Vec<f32>>    = Vec::new();
        // let mut normals: Vec<Vec<f32>>   = Vec::new();
        // let mut texcoord: Vec<Vec<f32>>  = Vec::new();
        // let mut indexes: Vec<Vec<Vec<u32>>> = Vec::new();
        // let mut vertindex: Vec<Vec<u32>> = Vec::new();
        // let mut normindex: Vec<Vec<u32>> = Vec::new();
        // let mut texcindex: Vec<Vec<u32>> = Vec::new();

        // for line in reader {
            // let elements: Vec<&str> = line.split(char::is_whitespace).collect();

            // if elements[0] == "v" {
                // vertex.push(
                    // elements.iter()
                        // .skip(1)
                        // .map(|x| x.parse::<f32>().unwrap())
                        // .collect()
                // );
            // } else if elements[0] == "vn" {
                // normals.push(
                    // elements.iter()
                        // .skip(1)
                        // .map(|x| x.parse::<f32>().unwrap())
                        // .collect()
                // );
            // } else if elements[0] == "vt" {
                // texcoord.push(
                    // elements.iter()
                        // .skip(1)
                        // .map(|x| x.parse::<f32>().unwrap())
                        // .collect()
                // );
            // } else if elements[0] == "f" {
                // indexes.push(
                    // elements.iter()
                        // .skip(1)
                        // .map(|x| x.split('/'))
                        // // .map(|x| if x == "" { ""})
                        // .map(|x| x.map(|a| a.parse::<u32>().unwrap()))
                        // .map(|x| x.collect::<Vec<u32>>())
                        // .collect()
                // );
            // }
        // }

        // vertindex = indexes.iter()
            // .map(|x| x.iter().map(|v| v[0]).collect::<Vec<u32>>())
            // .collect()
            // ;

        // texcindex = indexes.iter()
            // .map(|x| x.iter().map(|v| v[1]).collect::<Vec<u32>>())
            // .collect()
            // ;

        // normindex = indexes.iter()
            // .map(|x| x.iter().map(|v| v[2]).collect::<Vec<u32>>())
            // .collect()
            // ;

        // Ok(())
    // }
}
