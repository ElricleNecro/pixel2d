use std::ops::{Add, Sub};

use nalgebra as na;

use pixel_2d::render::{Vec3f32, Vec4f32};
use pixel_2d::resources::Error as RError;
use pixel2d_derive::VertexAttribPointers;

mod cube;
pub use cube::Cube;

pub mod sphere;

#[derive(Copy,Clone,Debug)]
#[derive(VertexAttribPointers)]
#[repr(C, packed)]
struct Vertex {
    #[location = 0]
    pos: Vec3f32,
    #[location = 1]
    color: Vec4f32,
}

impl Add<Vertex> for Vertex {
    type Output = Vertex;

    fn add(self, other: Vertex) -> Vertex {
        Vertex {
            pos: self.pos + other.pos,
            color: self.color,
        }
    }
}

impl Sub<Vertex> for Vertex {
    type Output = Vertex;

    fn sub(self, other: Vertex) -> Vertex {
        Vertex {
            pos: self.pos - other.pos,
            color: self.color,
        }
    }
}

pub trait Renderable {
    fn translate(&mut self, trans: &na::Matrix4<f32>);
    fn render(&mut self, context: *const pixel2d_gl:: Gl, projection: &na::Matrix4<f32>, view: &na::Matrix4<f32>, elapsed: f32) -> Result<(), RError>;
}
